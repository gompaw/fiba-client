**IN**
1 - Zawodnik na boisku
0 - W rezerwie
**AS**
Asysty
**BR**
?
**BS**
Blok
**ID**
id, zawsze rowne 0
**IND**
punkty zdobyte w czasie, gdy zawodnik był na boisku
**LAST_IN**
kiedy ostatnio był na boisku, w sekundach
**MIN**
ilość czasu na boisku , w sekundach
**NUMBER**
numer na koszulce
**P1A**
Rzuty za 1 punkt wykonane
**P1M**
Rzuty za 1 punkt celne
**P2A**
Rzuty za 2 punkty wykonane
**P2M**
Rzuty za 2 punkty celne
**P3A**
Rzuty za 3 punkty wykonane
**P3M**
Rzuty za 3 punkty celne
**PF**
Faule
**PTS**
punkty zdobyte przez zawodnika
**RD**
Zbiórka w obronie
**RF**
Faule otrzymane
**ST**
Przechwyty
**STA**
gracz wszedł na boisko na początku meczu (nie rezerwowy)
**TO**
Straty
**RO**
Zbiórka w ataku