from flask import Flask
from flask_restful import Api
from flask import redirect
from libs.Config import Config
from routes import routes
import webbrowser
from libs.casparcgcomm import CasparCGConnection

app = Flask(__name__)
api = Api(app)
socket = CasparCGConnection

@app.after_request
def after_request(response):
    response.headers.add('Access-Control-Allow-Origin', '*')
    response.headers.add('Access-Control-Allow-Headers', 'Content-Type,Authorization')
    response.headers.add('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE')
    return response


@app.route('/')
def main_view():
    return redirect('/static/index.html')


@app.route('/favicon.ico')
def favicon():
    return redirect('/static/favicon.ico')

for resource in routes:
    api.add_resource(resource, routes[resource])

if Config.get('auto_open_browser'):
    webbrowser.open('http://127.0.0.1:' + Config.get('port'))

if __name__ == '__main__':
    app.run(debug=True, port=int(Config.get('port')),host="0.0.0.0")

