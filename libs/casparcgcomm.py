import socket
from utils import common_error
from libs.Config import Config

def connect(host ,port):
    s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    try:
        s.connect((Config.get("casparCG_host") ,port))
    except Exception as e:
        return common_error(str(e))
    return s


class CasparCGConnection:

    s = connect('localhost',5250)

    @staticmethod
    def sendCommand(command):
        try:
          CasparCGConnection.s.send(command.encode('utf-8'))
        except Exception as e:
            return common_error(str(e))

    @staticmethod
    def addCG(channel, layer, template, data):
        command = 'CG ' + str(channel) + '-' + str(layer) + ' ADD 1 "' + template + '" 1 "'
        command += data.replace("\"","\\\"")
        command += '"\r\n'
        CasparCGConnection.sendCommand(command)

    @staticmethod
    def stopCG(channel, layer):
        command = 'CG ' + str(channel) + '-' + str(layer) + ' STOP 1\r\n'
        CasparCGConnection.sendCommand(command)

    @staticmethod
    def updateCG(channel,layer,data):
        command = 'CG ' + str(channel) + '-' + str(layer) + ' UPDATE 1 "'
        command += data.replace("\"","\\\"")
        command += '"\r\n'
        CasparCGConnection.sendCommand(command)

    @staticmethod
    def playVideo(channel, layer, template):
        command = 'PLAY ' + str(channel) + '-' + str(layer) + " " + template
        command += '"\r\n'
        CasparCGConnection.sendCommand(command)

    @staticmethod
    def stopVideo(channel, layer):
        command = 'PLAY ' + str(channel) + '-' + str(layer) + " EMPTY MIX 20"
        command += '"\r\n'
        CasparCGConnection.sendCommand(command)

    def layerOpacity(channel ,layer, opacity, time):
        command = 'MIXER ' + str(channel) + '-' + str(layer) + " OPACITY " + str(opacity) + " " + str(time)+ " linear"
        command += '"\r\n'
        CasparCGConnection.sendCommand(command)
