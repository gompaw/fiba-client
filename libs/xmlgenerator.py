from xml.etree.ElementTree import Element, SubElement, Comment, tostring
import json


class XmlGenerator:

    @staticmethod
    def get_xml(json_source: dict) -> str:
        mapped_fields = XmlGenerator.__map_fields(json_source)
        return(XmlGenerator.__generate_xml_string_from_mapped_fields(mapped_fields)).decode()

    @staticmethod
    def __map_fields(json_source: dict) -> dict:
        fields_after_mapping = {}
        with open("fields_to_xml_mappings.json") as mappings_file:
            mappings = json.load(mappings_file)
            for key in json_source:
                if key in mappings:
                    fields_after_mapping[mappings[key]] = str(json_source[key])
        return fields_after_mapping

    @staticmethod
    def __generate_xml_string_from_mapped_fields(fields: dict) -> str:
        parent_xml_element = Element('templateData')
        for single_field in fields:
            component_data_xml_element = SubElement(parent_xml_element, 'componentData', {"id": single_field})
            SubElement(component_data_xml_element, 'data', {"id": "text", "value": fields[single_field]})
        return tostring(parent_xml_element)