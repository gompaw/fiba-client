import json
from appdirs import user_data_dir


class Config:

    appdir = user_data_dir("Fiba", "gompaw")

    @classmethod
    def get(cls, key):
        if key in cls.__dict__:
            return cls.__dict__[key]
        with open('config.json') as data_file:
            data = json.load(data_file)
            return data[key]
