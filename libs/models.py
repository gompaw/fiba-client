class BaseModel():

    @classmethod
    def all(cls):
        return cls.to_arr(cls.select())

    @staticmethod
    def to_arr(queryset):
        return [obj.to_dict() for obj in queryset]

    @classmethod
    def from_dict(cls, json):
        obj = cls()
        for key in json.keys():
            setattr(obj, key, json[key])
        return obj

    @staticmethod
    def is_of_supported_type(value):
        if type(value) is str or type(value) is int or type(value) is bool or type(value) is float:
            return True
        return False

    def data_cleared_from_unsupported_fields(self):
        ret = {}
        for attr in self.__dict__:
            value = self.__dict__[attr]
            if BaseModel.is_of_supported_type(value):
                ret[attr] = value
            elif type(value) == list:
                ret[attr] = []
                for element in value:
                    ret[attr].append(element.to_dict())
            elif hasattr(value, "to_dict"):
                ret[attr] = value.to_dict()
        return ret

    def to_dict(self):
        return self.data_cleared_from_unsupported_fields()
