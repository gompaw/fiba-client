from libs.models import BaseModel
from models.player import Player
from models.stats import Stats
from dummy import get_dummy_player


class Team(BaseModel):

    def __str__(self):
        return self.name

    def __init__(self):
        self.name = ""
        self.coach = ""
        self.assistant_coaches = ""
        self.short_name = ""
        self.players = []
        self.stats = Stats()

    def append_player(self, player: Player):
        self.players.append(player)
