from os import makedirs, path, unlink
from libs.Config import Config


class HistoricalData:
    """
    Handles saving, loading and deleting history data from disk
    """

    @classmethod
    def save(cls, json_payload: str):
        instance = cls()
        instance._create_missing_dirs()
        instance._save(json_payload)

    @classmethod
    def delete(cls):
        instance = cls()
        unlink(instance._get_target_file_name())

    @classmethod
    def load(cls) ->str:
        instance = cls()
        return instance._load_history_or_throw_error()

    def __init__(self):
        self.target_path = Config.get('appdir')

    def _create_missing_dirs(self):
        try:
            makedirs(self.target_path)
        except FileExistsError:
            pass

    def _save(self, json_payload):
        with open(self._get_target_file_name(), "w") as fp:
            fp.write(json_payload)

    def _get_target_file_name(self):
        return path.join(
            self.target_path,
            "history.json"
        )

    def _load_history_or_throw_error(self):
        try:
            with open(self._get_target_file_name(), "r") as fp:
                return fp.read()
        except FileNotFoundError:
            raise HistoricalData.DoesNotExist("Dane historyczne nie zostały wgrane do programu")

    class DoesNotExist(Exception):
        pass
