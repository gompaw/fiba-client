from libs.models import BaseModel


class Stats(BaseModel):

    def __init__(self):
        self.assists = 0
        self.secs_on_field = 0
        self.net_points_while_on_court = 0
        self.last_sec_on_field = 0
        self.throws_1p_made = 0
        self.throws_1p_accurate = 0
        self.throws_2p_made = 0
        self.throws_2p_accurate = 0
        self.throws_3p_made = 0
        self.throws_3p_accurate = 0
        self.fouls_received = 0
        self.fouls = 0
        self.collections_on_defence = 0
        self.collections_on_offence = 0
        self.steals = 0
        self.loss = 0
        self.blocks = 0
