﻿from libs.models import BaseModel
import requests
from requests import exceptions
from xml.dom import minidom
from libs.Config import Config
from xml.parsers.expat import ExpatError


class ScoreBox(BaseModel):
    def __init__(self):
        try:
            r = requests.get(Config.get("referee_server_host"), timeout=1)
        except requests.exceptions.ConnectionError:
            raise ScoreBox.CannotDownloadScoreBoxInfo("Nie można pobrać danych z serwera sędziowskiego.")
        self.teamA = Team()
        self.teamB = Team()
        try:
            xmlDom = minidom.parseString(r.content)
        except ExpatError:
            raise ScoreBox.CannotDownloadScoreBoxInfo("Dane z serwera sędziowskiego mają niepoprawny format.")
        try:
            self.teamA.full_name = xmlDom.getElementsByTagName("NETCASTING")[0].getElementsByTagName("MATCHINFO")[0].getElementsByTagName("TEAM")[0].getAttribute("Name")
            self.teamA.short_name = xmlDom.getElementsByTagName("NETCASTING")[0].getElementsByTagName("MATCHINFO")[0].getElementsByTagName("TEAM")[0].getAttribute("Shortname")
            self.teamB.full_name = xmlDom.getElementsByTagName("NETCASTING")[0].getElementsByTagName("MATCHINFO")[0].getElementsByTagName("TEAM")[1].getAttribute("Name")
            self.teamB.short_name = xmlDom.getElementsByTagName("NETCASTING")[0].getElementsByTagName("MATCHINFO")[0].getElementsByTagName("TEAM")[1].getAttribute("Shortname")
            for quarter in \
                    xmlDom.getElementsByTagName("NETCASTING")[0].getElementsByTagName("BOXSCORE")[0].getElementsByTagName("QUARTERS")[0]._get_childNodes():
                quarter_score = QuarterScore()
                quarter_score.number = quarter.getAttribute('number')
                quarter_score.score = quarter.getAttribute('scoreA')
                self.teamA.quarters.append(quarter_score)
                quarter_score = QuarterScore()
                quarter_score.number = quarter.getAttribute('number')
                quarter_score.score = quarter.getAttribute('scoreB')
                self.teamB.quarters.append(quarter_score)
        except IndexError:
            raise ScoreBox.CannotDownloadScoreBoxInfo("Dane z serwera sędziowskiego mają niepoprawny format.")
        self.current_quarter_number = self.teamA.quarters.__len__()
        if self.teamA.quarters.__len__() > 4:
            self.current_quarter_name = "OT" + str(self.teamA.quarters.__len__() - 4)
        else:
            self.current_quarter_name = "Q" + str(self.teamA.quarters.__len__())

    class CannotDownloadScoreBoxInfo(Exception):
        pass


class QuarterScore(BaseModel):
    def __init__(self):
        self.number = ""
        self.score = ""


class Team(BaseModel):
    def __init__(self):
        self.quarters = []
        self.full_name = ""
        self.short_name = ""
