from libs.models import BaseModel
from models.stats import Stats


class Player(BaseModel):

    def __init__(self):
        self.first_name = ""
        self.last_name = ""
        self.position = ""
        self.birth_date = ""
        self.height = 0.0
        self.number = 0
        self.in_field = False
        self.stats = Stats()
        self.age = 0

    def __str__(self):
        return self.first_name + " " + self.last_name
