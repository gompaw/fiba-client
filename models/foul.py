from xml.dom import minidom
from xml.parsers.expat import ExpatError

import requests
from libs.Config import Config


class Foul:
    data = {}

    def __init__(self):
        Foul.data = {}
        try:
            r = requests.get(Config.get("referee_server_host"), timeout=1)
        except requests.exceptions.ConnectionError:
            raise Foul.CannotDownloadFoulsInfo("Nie można pobrać danych z serwera sędziowskiego.")
        try:
            xml_dom = minidom.parseString(r.content)
        except ExpatError:
            raise Foul.CannotDownloadFoulsInfo("Dane z serwera sędziowskiego mają niepoprawny format.")

        actions = xml_dom.getElementsByTagName("NETCASTING")[0].getElementsByTagName("ACTIONS")[0]

        for node in actions.childNodes:
            quart = node.getAttribute("Quarter")
            if quart not in Foul.data:
                Foul.data[quart] = {"1": 0, "2": 0}
            if self._is_node_a_foul(node):
                team = self._get_team_id(node)
                if quart not in Foul.data:
                    Foul.data[quart] = {"1": 0, "2": 0}
                Foul.data[quart][team] += 1


    @staticmethod
    def _get_team_id(action_node):
        for param in action_node.childNodes:
            if param.getAttribute("Name") == "Team":
                return param.getAttribute("Data")

    def _is_node_a_foul(self, node):
        return node.getAttribute("ShortName") == "foul"

    class CannotDownloadFoulsInfo(Exception):
        pass

