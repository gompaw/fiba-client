from libs.models import BaseModel
from models.team import Team
from models.player import Player
import requests
from requests import exceptions
from xml.dom import minidom
from libs.Config import Config
from xml.parsers.expat import ExpatError
import datetime


class Match(BaseModel):
    def __init__(self):
        try:
            r = requests.get(Config.get("referee_server_host"), timeout=1)
        except requests.exceptions.ConnectionError:
            raise Match.CannotDownloadMatchInfo("Nie można pobrać danych z serwera sędziowskiego.")
        try:
            xmlDom = minidom.parseString(r.content)
        except ExpatError:
            raise Match.CannotDownloadMatchInfo("Dane z serwera sędziowskiego mają niepoprawny format.")
        try:
            info = xmlDom.getElementsByTagName("NETCASTING")[0].getElementsByTagName("MATCHINFO")[0]
        except IndexError:
            raise Match.CannotDownloadMatchInfo("Dane z serwera sędziowskiego mają niepoprawny format.")
        self.place = info.getAttribute("Place")
        self.time = info.getAttribute("Time")
        self.field = info.getAttribute("Field")
        self.commissioner = info.getAttribute("Commissioner")
        self.referee_1 = info.getAttribute("Referee1")
        self.referee_2 = info.getAttribute("Referee2")
        self.referee_3 = info.getAttribute("Referee3")
        self.competition = info.getAttribute("Competition")
        self.number = info.getAttribute("GameNumber")
        info_teams = []
        try:
            info_teams.append(xmlDom.getElementsByTagName("NETCASTING")[0].getElementsByTagName("MATCHINFO")[0].getElementsByTagName("TEAM")[0])
            info_teams.append(xmlDom.getElementsByTagName("NETCASTING")[0].getElementsByTagName("MATCHINFO")[0].getElementsByTagName("TEAM")[1])
        except IndexError:
            raise Match.CannotDownloadMatchInfo("Dane z serwera sędziowskiego mają niepoprawny format.")
        teams = [Team(), Team()]

        for i in [0, 1]:
            teams[i].coach = info_teams[i].getAttribute("Coach")
            teams[i].assistant_coaches = info_teams[i].getAttribute("AssistantCoaches")
            teams[i].name = info_teams[i].getAttribute("Name").upper()
            teams[i].short_name = info_teams[i].getAttribute("Shortname")
            for player_info in info_teams[i].childNodes:
                player = Player()
                try:
                    player.height = float(player_info.getAttribute("Height"))
                except ValueError:
                    pass
                player.first_name = player_info.getAttribute("Name")
                player.number = int(player_info.getAttribute("Number"))
                player.birth_date = player_info.getAttribute("Birth_date")
                if player_info.getAttribute("Birth_date"):
                    player.age = int((datetime.date.today() - datetime.datetime.strptime(player_info.getAttribute("Birth_date"), "%Y.%m.%d").date()).days/365)
                player.last_name = player_info.getAttribute("Surname").upper()
                player.position = player_info.getAttribute("Position")
                teams[i].append_player(player)
        for i in [0, 1]:
            for player in teams[i].players:
                for temp in xmlDom.getElementsByTagName("NETCASTING")[0].getElementsByTagName("BOXSCORE")[0].getElementsByTagName("TEAM")[i].childNodes:
                    if int(temp.getAttribute("Number")) == player.number:
                        player.stats.assists = int(temp.getAttribute("AS"))
                        player.stats.throws_1p_accurate = int(temp.getAttribute("P1M"))
                        player.stats.throws_1p_made = int(temp.getAttribute("P1A"))
                        player.stats.throws_2p_accurate = int(temp.getAttribute("P2M"))
                        player.stats.throws_2p_made = int(temp.getAttribute("P2A"))
                        player.stats.throws_3p_accurate = int(temp.getAttribute("P3M"))
                        player.stats.throws_3p_made = int(temp.getAttribute("P3A"))
                        player.stats.collections_on_defence = int(temp.getAttribute("RD"))
                        player.stats.collections_on_offence = int(temp.getAttribute("RO"))
                        player.stats.fouls = int(temp.getAttribute("PF"))
                        player.stats.fouls_received = int(temp.getAttribute("RF"))
                        player.stats.loss = int(temp.getAttribute("TO"))
                        player.stats.steals = int(temp.getAttribute("ST"))
                        player.stats.secs_on_field = int(temp.getAttribute("MIN"))
                        player.stats.last_sec_on_field = int(temp.getAttribute("LAST_IN"))
                        player.stats.net_points_while_on_court = int(temp.getAttribute("IND"))
                        player.stats.blocks = int(temp.getAttribute("BS"))
                        player.in_field = True if temp.getAttribute("IN") == "1" else False

        for i in [0, 1]:
                for temp in xmlDom.getElementsByTagName("NETCASTING")[0].getElementsByTagName("BOXSCORE")[0].getElementsByTagName("TEAM")[i].childNodes:
                    if temp.getAttribute("Number") == "-2":
                        teams[i].stats.assists = int(temp.getAttribute("AS"))
                        teams[i].stats.throws_1p_accurate = int(temp.getAttribute("P1M"))
                        teams[i].stats.throws_1p_made = int(temp.getAttribute("P1A"))
                        teams[i].stats.throws_2p_accurate = int(temp.getAttribute("P2M"))
                        teams[i].stats.throws_2p_made = int(temp.getAttribute("P2A"))
                        teams[i].stats.throws_3p_accurate = int(temp.getAttribute("P3M"))
                        teams[i].stats.throws_3p_made = int(temp.getAttribute("P3A"))
                        teams[i].stats.collections_on_defence = int(temp.getAttribute("RD"))
                        teams[i].stats.collections_on_offence = int(temp.getAttribute("RO"))
                        teams[i].stats.fouls = int(temp.getAttribute("PF"))
                        teams[i].stats.fouls_received = int(temp.getAttribute("RF"))
                        teams[i].stats.loss = int(temp.getAttribute("TO"))
                        teams[i].stats.steals = int(temp.getAttribute("ST"))
                        teams[i].stats.secs_on_field = int(temp.getAttribute("MIN"))
                        teams[i].stats.last_sec_on_field = int(temp.getAttribute("LAST_IN"))
                        teams[i].stats.net_points_while_on_court = int(temp.getAttribute("IND"))
                        teams[i].stats.blocks = int(temp.getAttribute("BS"))

        self.teamA = teams[0]
        self.teamB = teams[1]

    class CannotDownloadMatchInfo(Exception):
        pass
