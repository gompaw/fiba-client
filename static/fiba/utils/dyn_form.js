'use strict';

angular.module('fibaclient.utils.dyn_form',[])
    .factory('DynForm',
    ['$timeout', function($timeout) {

        var self = this;

        self.attachFieldRecognitionMethods = function(scope){
            scope.isText = function(field){
                return field.type == "text";
            };

            scope.isNumber = function(field){
                return field.type == "number";
            };

            scope.isSelect = function(field){
                return field.type == "select";
            };

            scope.isBool = function(field){
                return field.type == "bool";
            };
        };

        return {
            attachTo : function(scope, model){
                self.attachFieldRecognitionMethods(scope);
                scope.data = model;
            }
        }
    }]);