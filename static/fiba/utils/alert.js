'use strict';

angular.module('fibaclient.alert',[])
    .factory('Alert',
    ['$timeout', function($timeout) {
        var self = this;
        self.queue = [];

        self.getMessageIndexById = function(id){
            for(var i = 0 ; i < self.queue.length ; i++){
                if(self.queue[i].id === id){
                    return i;
                }
            }
        };

        self.getUniqueId = function(){
            var uid = Math.random();
            if(self.getMessageIndexById(uid) !== undefined){
                return self.getUniqueId();
            }
            return uid;
        };


        self.addMessage = function(message, type, delay){
            var uid = self.getUniqueId();
            self.queue.unshift({
                id: uid,
                message: message,
                type: type
            });
            if(delay !== undefined){
                $timeout(function(){
                    self.queue.splice(self.getMessageIndexById(uid),1);
                }, delay);
            }
        };




        return {
            addError: function(message, delay){
                self.addMessage(message, 'danger', delay);
            },
            addWarning: function(message, delay){
                self.addMessage(message, 'warning', delay);
            },
            addInfo: function(message, delay){
                self.addMessage(message, 'info', delay);
            },
            addSuccess: function(message, delay){
                self.addMessage(message, 'success', delay);
            },
            getMessages: function(){
                return self.queue;
            },
            close: function(index){
                self.queue.splice(index, 1);
            }

        }
    }]);