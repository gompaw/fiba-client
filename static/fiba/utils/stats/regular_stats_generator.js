'use strict';

angular.module('fibaclient.utils.stats_generator.regular', [])
    .factory('RegularStatsGenerator',
    ['StatsGenerator', function (StatsGenerator) {
        var self = StatsGenerator;

        self.percentGenerator = function(data){
            var key = self.getSortingMethodByName(self.stat_type).remote_stat_name;
            var key_made = self.getSortingMethodByName(self.stat_type).remote_stat_name.replace('accurate', 'made');
            var key_accurate = self.getSortingMethodByName(self.stat_type).remote_stat_name.replace('made', 'accurate' );

            for(var i = 0 ; i < self.data.a.stats.length ; i++){
                if(self.data.a.stats[i][key_accurate] != 0){
                    data['playerA' + (i+1) +'.stat'] =  self.data.a.stats[i][key_accurate] + "/" + self.data.a.stats[i][key_made];
                    data['playerA' + (i+1) +'.stat_percent'] =  (self.data.a.stats[i][key_accurate]/self.data.a.stats[i][key_made] * 100).toFixed(1) + "%";
                    data['playerA' + (i+1) +'.name'] =  self.data.a.stats[i]['first_name'];
                    data['playerA' + (i+1) +'.firstletter'] =  self.data.a.stats[i]['first_name'][0];
                    data['playerA' + (i+1) +'.surname'] =  self.data.a.stats[i]['last_name'];
                    data['playerA' + (i+1) +'.surname'] =  self.data.a.stats[i]['last_name'];
                    data['playerA' + (i+1) +'.number'] =  self.data.a.stats[i]['number'];
                }else{
                    data['playerA' + (i+1) +'.stat'] = ' ';
                    data['playerA' + (i+1) +'.stat_percent'] = ' ';
                    data['playerA' + (i+1) +'.name'] = ' ';
                    data['playerA' + (i+1) +'.firstletter'] = ' ';
                    data['playerA' + (i+1) +'.surname'] = ' ';
                    data['playerA' + (i+1) +'.number'] = ' ';
                }

            }
            for(var i = 0 ; i < self.data.b.stats.length ; i++){
                if(self.data.b.stats[i][key_accurate] != 0){
                    data['playerB' + (i+1) +'.stat'] =  self.data.b.stats[i][key_accurate] + "/" + self.data.b.stats[i][key_made];
                    data['playerB' + (i+1) +'.stat_percent'] =  (self.data.b.stats[i][key_accurate]/self.data.b.stats[i][key_made] * 100).toFixed(1) + "%";
                    data['playerB' + (i+1) +'.name'] =  self.data.b.stats[i]['first_name'];
                    data['playerB' + (i+1) +'.firstletter'] =  self.data.b.stats[i]['first_name'][0];
                    data['playerB' + (i+1) +'.surname'] =  self.data.b.stats[i]['last_name'];
                    data['playerB' + (i+1) +'.number'] =  self.data.b.stats[i]['number'];
                }else{
                    data['playerB' + (i+1) +'.stat'] = ' ';
                    data['playerB' + (i+1) +'.stat_percent'] = ' ';
                    data['playerB' + (i+1) +'.name'] = ' ';
                    data['playerB' + (i+1) +'.firstletter'] = ' ';
                    data['playerB' + (i+1) +'.surname'] = ' ';
                    data['playerB' + (i+1) +'.number'] = ' ';
                }

            }
        };

        self.numericGenerator = function(data){
            var key = self.getSortingMethodByName(self.stat_type).remote_stat_name;
            for(var i = 0 ; i < self.data.a.stats.length ; i++){
                if(parseInt(self.data.a.stats[i][key]) == 0){
                    data['playerA' + (i+1) +'.stat'] = ' ';
                    data['playerA' + (i+1) +'.name'] = ' ';
                    data['playerA' + (i+1) +'.firstletter'] = ' ';
                    data['playerA' + (i+1) +'.surname'] = ' ';
                    data['playerA' + (i+1) +'.number'] = ' ';
                }else{
                    data['playerA' + (i+1) +'.stat'] =  self.data.a.stats[i][key];
                    data['playerA' + (i+1) +'.name'] =  self.data.a.stats[i]['first_name'];
                    data['playerA' + (i+1) +'.surname'] =  self.data.a.stats[i]['last_name'];
                    data['playerA' + (i+1) +'.firstletter'] =  self.data.a.stats[i]['first_name'][0];
                    data['playerA' + (i+1) +'.number'] =  self.data.a.stats[i]['number'];
                }

            }
            for(var i = 0 ; i < self.data.b.stats.length ; i++){
                if(parseInt(self.data.b.stats[i][key]) == 0){
                    data['playerB' + (i+1) +'.stat'] = ' ';
                    data['playerB' + (i+1) +'.name'] = ' ';
                    data['playerB' + (i+1) +'.firstletter'] = ' ';
                    data['playerB' + (i+1) +'.surname'] = ' ';
                    data['playerB' + (i+1) +'.number'] = ' ';
                }else{
                    data['playerB' + (i+1) +'.stat'] =  self.data.b.stats[i][key];
                    data['playerB' + (i+1) +'.name'] =  self.data.b.stats[i]['first_name'];
                    data['playerB' + (i+1) +'.surname'] =  self.data.b.stats[i]['last_name'];
                    data['playerB' + (i+1) +'.firstletter'] =  self.data.b.stats[i]['first_name'][0];
                    data['playerB' + (i+1) +'.number'] =  self.data.b.stats[i]['number'];
                }

            }
        };

        self.getStats = function(){
            var data = StatsGenerator.getCommonStatParams();
            self[self.getSortingMethodByName(self.stat_type).generator + 'Generator'](data);
            return data;
        };

        return self;

    }]);