'use strict';

angular.module('fibaclient.utils.stats_generator', [])
    .factory('StatsGenerator',
    [function () {

        var self = {data: {}};


        self.initialize = function(config){

        };

        self.compare = function(a,b){
            if(a==b){
                return 0;
            }
            return a > b;
        };

        self.getSumOfPoints = function(player){
            player.points_accurate = player['stats.throws_1p_accurate']+
                player['stats.throws_2p_accurate']*2 +
                player['stats.throws_3p_accurate']*3;
            return player.points_accurate;
        };

        self.getSortingMethodByName = function(searched_name){
            for(var i = 0 ; i < self.stats_definition.length ; i ++){
                if(self.stats_definition[i].name ==searched_name ){
                    return self.stats_definition[i];
                }
            }
            return undefined;
        };

        self.getSortingMethodLabelByName = function(searched_name){
            return self.getSortingMethodByName(searched_name).label;
        };


        self.pointsComparator = function(player1, player2){
            return self.getSumOfPoints(player2) - self.getSumOfPoints(player1);
        };

        self.freeShotsComparator = function(player1, player2){
            return player2['stats.throws_1p_accurate'] - player1['stats.throws_1p_accurate'];
        };

        self.twoPointsComparator = function(player1, player2){
            return player2['stats.throws_2p_accurate'] - player1['stats.throws_2p_accurate'];
        };

        self.threePointsComparator = function(player1, player2){
            return player2['stats.throws_3p_accurate'] - player1['stats.throws_3p_accurate'];
        };

        self.interceptionsComparator = function(player1, player2){
            return player2['stats.steals'] - player1['stats.steals'];
        };

        self.losesComparator = function(player1, player2){
            return player2['stats.loss'] - player1['stats.loss'];
        };

        self.foulsComparator = function(player1, player2){
            return player2['stats.fouls'] - player1['stats.fouls'];
        };

        self.stats_definition = [
            {
                name: 'free_shots',
                remote_stat_name: 'stats.throws_1p_accurate',
                label: 'Rzuty wolne',
                name_for_collection: 'players_by_free_shots',
                comparator: self.freeShotsComparator,
                generator: 'percent'
            },
            {
                name: '2_point_shots',
                remote_stat_name: 'stats.throws_2p_accurate',
                label: 'Rzuty za 2',
                name_for_collection: 'players_by_2p_shots',
                comparator: self.twoPointsComparator,
                generator: 'percent'
            },
            {
                name: '3_point_shots',
                label: 'Rzuty za 3',
                remote_stat_name: 'stats.throws_3p_accurate',
                name_for_collection: 'players_by_3p_shots',
                comparator: self.threePointsComparator,
                generator: 'percent'
            },
            {
                name: 'total_points',
                label: 'Liczba zdobytych punktów',
                name_for_collection: 'players_by_points',
                remote_stat_name: 'points_accurate',
                comparator: self.pointsComparator,
                generator: 'numeric'
            },
            {
                name: 'interceptions',
                label: 'Przechwyty',
                name_for_collection: 'players_by_interceptions',
                remote_stat_name: 'stats.steals',
                comparator: self.interceptionsComparator,
                generator: 'numeric'
            },
            {
                name: 'fouls',
                label: 'Faule',
                name_for_collection: 'players_by_fouls',
                remote_stat_name: 'stats.fouls',
                comparator: self.foulsComparator,
                generator: 'numeric'
            },
            {
                name: 'loses',
                label: 'Straty',
                name_for_collection: 'players_by_loses',
                remote_stat_name: 'stats.loss',
                comparator: self.losesComparator,
                generator: 'numeric'
            }
        ];

        self.setData = function(data, stat_type){
            self.data = data;
            self.stat_type = stat_type;
        };

        self.getCommonStatParams = function(){

            var total_points_a = 0;
            for(var i = 0 ; i < self.data.a.team.players.length ; i++){

                total_points_a += self.data.a.team.players[i]["stats.throws_1p_accurate"];
                total_points_a += self.data.a.team.players[i]["stats.throws_2p_accurate"] * 2;
                total_points_a += self.data.a.team.players[i]["stats.throws_3p_accurate"] * 3;
            }
            total_points_a += self.data.a.team.stats.throws_1p_accurate;
            total_points_a += self.data.a.team.stats.throws_2p_accurate * 2;
            total_points_a += self.data.a.team.stats.throws_3p_accurate * 3;

            var total_points_b = 0;
            for(var i = 0 ; i < self.data.b.team.players.length ; i++){

                total_points_b += self.data.b.team.players[i]["stats.throws_1p_accurate"];
                total_points_b += self.data.b.team.players[i]["stats.throws_2p_accurate"] * 2;
                total_points_b += self.data.b.team.players[i]["stats.throws_3p_accurate"] * 3;
            }
            total_points_b += self.data.b.team.stats.throws_1p_accurate;
            total_points_b += self.data.b.team.stats.throws_2p_accurate * 2;
            total_points_b += self.data.b.team.stats.throws_3p_accurate * 3;


            return {
                'stat.name' : self.getSortingMethodLabelByName(self.stat_type),
                'statA.points' : total_points_a,
                'statB.points' : total_points_b,
                'teamA.shortname' : self.data.a.team.short_name,
                'teamA.name' : self.data.a.team.name,
                'teamB.shortname' : self.data.b.team.short_name,
                'teamB.name' : self.data.b.team.name
            }
        };

        return self;

    }]);