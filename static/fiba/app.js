'use strict';


angular.module('FibaClient', [
  'ngResource',
  'ui.bootstrap',
  'fibaclient.controllers.main',
  'fibaclient.controllers.config',
  'fibaclient.config',
  'fibaclient.alert',
  'fibaclient.utils.dyn_form',
  'fibaclient.utils.stats_generator',
  'fibaclient.utils.stats_generator.percent',
  'fibaclient.utils.stats_generator.regular',

  'fibaclient.signals.update_squad',
  'fibaclient.signals.update_on_pitch',

  'fibaclient.controllers.fields.renderer',
  'fibaclient.controllers.fields.text',
  'fibaclient.controllers.fields.bool',
  'fibaclient.controllers.fields.number',

  'fibaclient.controllers.history',

  'fibaclient.controllers.team',
  'fibaclient.controllers.team.bars.coach',
  'fibaclient.controllers.team.bars.squad',

  'fibaclient.controllers.pitch',
  'fibaclient.controllers.on_pitch',

  'fibaclient.controllers.score',
  'fibaclient.controllers.combo',
  'fibaclient.controllers.scorebox',
  'fibaclient.controllers.stats',
  'fibaclient.controllers.stats.compare',
  'fibaclient.controllers.stats.team',
  'fibaclient.controllers.stats.top',

  'fibaclient.controllers.player',
  'fibaclient.controllers.player.bars.individual',
    
  'fibaclient.controllers.referee',
  'fibaclient.controllers.mode',
  'fibaclient.models.match',
  'fibaclient.models.score'
]);