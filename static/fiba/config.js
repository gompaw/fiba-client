'use strict';

angular.module('fibaclient.config',[])
    .factory('Config',
    ['$resource', '$interval',
        function($resource, $interval) {

            var self = this;

            self.defaults = {
                'theme': 'fiba/styles/themes/paper.css'
            };

            return {
                setValue: function(key, value){
                    try {
                        localStorage.setItem(key, JSON.stringify(value));
                    }catch(e){

                    }
                },
                getValue: function(key){
                    try{
                        var ret = localStorage.getItem(key);
                        if(ret !== undefined && ret !== null){
                            return JSON.parse(ret);
                        }
                        return self.defaults[key];
                    }catch(e){
                        return undefined;
                    }
                }

            }
        }]);