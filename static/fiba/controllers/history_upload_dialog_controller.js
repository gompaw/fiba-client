'use strict';

angular.module('fibaclient.controllers.history', [
        'ngResource'
    ])
    .controller('HistoryUploadButtonController', ['$scope', '$modal', function($scope, $modal) {

        $scope.openModal = function(){
            var modalInstance = $modal.open({
                animation: false,
                templateUrl: 'fiba/templates/upload/dialog.html',
                controller: 'HistoryUploadDialogController',
                size: 'lg',
                resolve: {
                    config: function () {
                        return {
                        };
                    }
                }
            });

            modalInstance.result.then(function (selectedItem) {

            }, function () {

            });
        };


    }]).
controller('HistoryUploadDialogController', function ($scope,  Match,  $modalInstance, $http, Alert, MappingHistoryData) {

    $scope.parsed_history = undefined;
    $scope.empty_teams = {
        history: undefined,
        team_a_id: undefined,
        team_b_id: undefined
    };
    $scope.teams = $scope.empty_teams;


    $scope.showContent = function($fileContent){
        $scope.content = $fileContent;
    };

    $scope.$watch('content', function(new_value){
        if(new_value){
            $scope.parsed_history = JSON.parse(new_value);
        }
    });

    $scope.autoloadParsedHistoryFromServer = function(){
        $http({
            url: '/history',
            method: 'GET'
        }).success(function(data){
            $scope.parsed_history = data;
        }).error(function(data){
            Alert.addError('Nie udało się pobrać danych historycznych z komputera:  ' + data.message, 4000)
        });

        var saved_mapping = MappingHistoryData.get();

        if(saved_mapping){
            $scope.teams = saved_mapping;
        }
    };

    $scope.saveOnServer = function(){

        $scope.empty_teams = {
            history: undefined,
            team_a_id: undefined,
            team_b_id: undefined
        };
        var not_null_validations = [$scope.parsed_history, $scope.teams.team_a_id, $scope.teams.team_b_id];
        console.log(not_null_validations);
        for(var i=0; i<not_null_validations.length;i++){
            if(not_null_validations[i] == undefined){
                Alert.addError('Niepoprawny zestaw danych lub niewypełnione jedno z mapowań.', 4000);
                return;
            }
        }

        $http({
            url: '/history',
            method: 'POST',
            data: $scope.parsed_history
        }).success(function(){
            $scope.saveMappings();
            Alert.addSuccess('Dane historyczne zapisano na komputerze.', 2000)
        }).error(function(data){
            Alert.addError('Nie udało się zapisać danych historycznych na komputerze:  ' + data.message, 4000)
        });
    };

    $scope.deleteFromServer = function(){
        $http({
            url: '/history',
            method: 'DELETE'
        }).success(function(){
            Alert.addSuccess('Dane historyczne usunięto z komputera.', 2000);
            $scope.parsed_history = undefined;
            $scope.clearMappings();
        }).error(function(data){
            Alert.addError('Nie udało się usunąć danych historycznych z komputera:  ' + data.message, 4000)
        });
    };

    $scope.clearMappings = function(){
        MappingHistoryData.clear();
        $scope.teams = JSON.parse(JSON.stringify($scope.empty_teams));
    };

    $scope.saveMappings = function(){
        $scope.teams['history'] = $scope.parsed_history;
        MappingHistoryData.save($scope.teams);
    };

    $scope.refreshTeams = function(){

    };

    $scope.autoloadParsedHistoryFromServer();


}).directive('onReadFile', function ($parse) {
    return {
        restrict: 'A',
        scope: false,
        link: function(scope, element, attrs) {
            var fn = $parse(attrs.onReadFile);

            element.on('change', function(onChangeEvent) {
                var reader = new FileReader();

                reader.onload = function(onLoadEvent) {
                    scope.$apply(function() {
                        fn(scope, {$fileContent:onLoadEvent.target.result});
                    });
                };

                reader.readAsText((onChangeEvent.srcElement || onChangeEvent.target).files[0]);
            });
        }
    };
}).factory('MappingHistoryData',
    [
        function() {
            var self = this;

            self.save = function(data){
                localStorage.setItem('history_', JSON.stringify(data));
            };

            self.get = function(){
                return JSON.parse(localStorage.getItem('history_'));
            };

            self.clear = function(){
                localStorage.removeItem('history_');
            };

            return self;
        }]).factory('HistoryForSmallStatistics',
    [ "MappingHistoryData",
        function(MappingHistoryData) {
            var self = this;

            self.full_data = undefined;
            // dla malych belek
            self.team_a_data = undefined;
            // dla srednich i duzych belek
            self.team_a_data_big = undefined;

            self.team_b_data = undefined;
            self.team_b_data_big = undefined;

            self.refresh_data = function(){
                self.full_data = MappingHistoryData.get();
                for(var i = 0 ; i < self.full_data.history.length; i++){
                    var current_team = self.full_data.history[i];
                    if (current_team.teamid == self.full_data.team_a_id){
                        self.team_a_data = self.small_bar_normalize(current_team);
                        self.team_a_data_big = self.big_bar_normalize(current_team, "A");
                    }
                    if (current_team.teamid == self.full_data.team_b_id){
                        self.team_b_data = self.small_bar_normalize(current_team);
                        self.team_b_data_big = self.big_bar_normalize(current_team, "B");
                    }
                }
            };

            self.small_bar_normalize = function(raw_esok_team_data){
                return {
                    "stats.throws_1p_accurate": raw_esok_team_data["Proc1"] + "%",
                    "stats.throws_1p_accurate_percentage": raw_esok_team_data["Proc1"] + "%",
                    "stats.throws_2p_accurate": raw_esok_team_data["Proc2"] + "%",
                    "stats.throws_2p_accurate_percentage": raw_esok_team_data["Proc2"] + "%",
                    "stats.throws_3p_accurate": raw_esok_team_data["Proc3"] + "%",
                    "stats.throws_3p_accurate_percentage": raw_esok_team_data["Proc3"] + "%",
                    "stats.steals": (raw_esok_team_data["P"])+"",
                    "stats.loss": (raw_esok_team_data["S"])+"",
                    "stats.blocks": (raw_esok_team_data["B"])+"",
                    "stats.fouls": (raw_esok_team_data["F"])+"",
                    "stats.collections": (parseInt(raw_esok_team_data["A"])+parseInt(raw_esok_team_data["O"]))+"",
                    "stats.collections_on_defence": parseInt(raw_esok_team_data["O"])+"",
                    "stats.collections_on_offence": parseInt(raw_esok_team_data["A"])+"",
                    "stats.assists": (raw_esok_team_data["As"])+""
                };
            };

            self.big_bar_normalize = function(raw_esok_team_data, team){
                var stats = {};
                stats["stat"+team+".assists"] = (raw_esok_team_data["As"])+"";
                stats["stat"+team+".blocks"] = (raw_esok_team_data["B"])+"";
                stats["stat"+team+".points"] = (raw_esok_team_data["Pkt"])+"";
                stats["stat"+team+".collections"] = (raw_esok_team_data["Sum"]);
                stats["stat"+team+".collections_string"] = parseInt(raw_esok_team_data["A"])+"/"+parseInt(raw_esok_team_data["O"]);
                stats["stat"+team+".collections_on_defence"] = parseInt(raw_esok_team_data["O"])+"";
                stats["stat"+team+".collections_on_offence"] = parseInt(raw_esok_team_data["A"])+"";
                stats["stat"+team+".steals"] = (raw_esok_team_data["P"])+"";
                stats["stat"+team+".loss"] = (raw_esok_team_data["S"])+"";
                stats["stat"+team+".fouls"] = (raw_esok_team_data["F"])+"";
                stats["stat"+team+".throws_1p"] =  parseInt(raw_esok_team_data["c1"]) + "/" + parseInt(raw_esok_team_data["w1"]);
                stats["stat"+team+".throws_1p_percentage"] =  raw_esok_team_data["Proc1"] + "%";
                stats["stat"+team+".throws_2p"] =  parseInt(raw_esok_team_data["c2"]) + "/" + parseInt(raw_esok_team_data["w2"]);
                stats["stat"+team+".throws_2p_percentage"] =  raw_esok_team_data["Proc2"] + "%";
                stats["stat"+team+".throws_3p"] =  parseInt(raw_esok_team_data["c3"]) + "/" + parseInt(raw_esok_team_data["w3"]);
                stats["stat"+team+".throws_3p_percentage"] =  raw_esok_team_data["Proc3"] + "%";
                return stats;
            };

            self.is_available = function(){
                self.full_data = MappingHistoryData.get();
                //to bool
                return !(!self.full_data);
            };

            self.refresh_data();

            return self;
        }]).factory('HistoryForPlayer',
    [ "MappingHistoryData",
        function(MappingHistoryData) {
            var self = this;


            self.refresh_data = function(team_letter, player_number){
                self.player_data = {};
                var full_data = MappingHistoryData.get();
                var team_id;
                if(team_letter == 'a'){
                    team_id =full_data.team_a_id;
                }else{
                    team_id =full_data.team_b_id;
                }

                var team = null;
                for(var i=0 ; i < full_data.history.length ; i++){
                    if(full_data.history[i].teamid == team_id){
                        team = full_data.history[i];
                        break;
                    }
                }
                var player = null;
                for(var j=0 ; j < team.players.length ; j++){
                    if(team.players[j].numer == player_number){
                        player = team.players[j];
                        break;
                    }
                }


                if(!player){
                    self.player_data = undefined;
                    return false;
                }else{
                     self.player_data = {};
                }

                console.log("Attaching to historical data:");
                console.log(player);

                try{
                    self.player_data["stats.assists"] = parseInt(player["As"])+"";
                    self.player_data["stats.blocks"] = parseInt(player["B"])+"";
                    self.player_data["stats.points"] = parseInt(player["Pkt"])+"";
                    self.player_data["stats.collections"] = parseInt(player["Sum"]);
                    self.player_data["stats.collections_string"] = parseInt(player["A"])+"/"+parseInt(player["O"]);
                    self.player_data["stats.collections_on_defence"] = parseInt(player["O"])+"";
                    self.player_data["stats.collections_on_offence"] = parseInt(player["A"])+"";
                    self.player_data["stats.steals"] = parseInt(player["P"])+"";
                    self.player_data["stats.loss"] = parseInt(player["S"])+"";
                    self.player_data["stats.fouls"] = parseInt(player["F"])+"";
                    self.player_data["stats.throws_1p"] =  parseInt(player["c1"]) + "/" + parseInt(player["w1"]);
                    self.player_data["stats.throws_1p_percentage"] =  player["Proc1"] + "%";
                    self.player_data["stats.throws_2p"] =  parseInt(player["c2"]) + "/" + parseInt(player["w2"]);
                    self.player_data["stats.throws_2p_percentage"] =  player["Proc2"] + "%";
                    self.player_data["stats.throws_3p"] =  parseInt(player["c3"]) + "/" + parseInt(player["w3"]);
                    self.player_data["stats.throws_3p_percentage"] =  player["Proc3"] + "%";
                }catch(e){
                    console.log(e);
                    self.player_data = undefined;
                    return false;

                }



            };



            return self;
        }]);