'use strict';

angular.module('fibaclient.controllers.player', [
    'ngResource'
])
    .controller('PlayerController', ['$scope', '$modal', function($scope, $modal) {

        $scope.player = {};


        $scope.initFor = function(player){
            $scope.player = player;
            $scope.player_model = new Player($scope.player);
            $scope.player_model.team = $scope.$parent.team;
        };

        $scope.openModal = function(limit_fields){
            var modalInstance = $modal.open({
                animation: false,
                templateUrl: 'fiba/templates/player/bars/player_individual.html',
                controller: 'PlayerIndividualBarController',
                size: 'lg',
                resolve: {
                    config: function () {
                        return {
                            player: $scope.player_model,
                            limit_fields: limit_fields,
                            team: $scope.$parent.team,
                            team_identifier: $scope.$parent.team_identificator,
                        };
                    }
                }
            });

            modalInstance.result.then(function (selectedItem) {

            }, function () {

            });
        };

        $scope.getButtonClass = function(){
            return $scope.player_model.isOnPitch()?'primary':'default';
        }

    }]);