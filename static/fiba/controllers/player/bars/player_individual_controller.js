'use strict';

angular.module('fibaclient.controllers.player.bars.individual', [
    'ngResource'
]).
controller('PlayerIndividualBarController', function ($scope,  Match,  $modalInstance, config, DynForm, $http, Alert, $interval, HistoryForPlayer) {

    $scope.auto_refresh = false;
    HistoryForPlayer.refresh_data(config.team_identifier, config.player.data.number);
    $scope.history = HistoryForPlayer.player_data;
    $scope.attach_history_data = false;




    $scope.BAR_TYPE_STATS = 1;
    $scope.BAR_TYPE_PERSONAL_SHOT = 2;
    $scope.BAR_TYPE_FOULS = 3;
    $scope.BAR_TYPE_DATA = 4;
    $scope.BAR_TYPE_POINTS = 5;
    $scope.BAR_TYPE_POINTS2 = 6;

    $scope.BAR_TYPES = [
        {
            id: $scope.BAR_TYPE_STATS,
            label: "Podpisowa"
        },
        {
            id: $scope.BAR_TYPE_PERSONAL_SHOT,
            label: "Rzuty osobiste"
        },
        {
            id: $scope.BAR_TYPE_FOULS,
            label: "Faule"
        },
        {
            id: $scope.BAR_TYPE_DATA,
            label: "Informacje"
        },
        {
            id: $scope.BAR_TYPE_POINTS,
            label: "Pkt/Zb/Bl/F"
        },
        {
            id: $scope.BAR_TYPE_POINTS2,
            label: "Pkt/A/P/S"
        }
    ];

    $scope.player = config.player;
    $scope.team = config.team;

    DynForm.attachTo($scope, $scope.player.data);

    $scope.bar_type = $scope.BAR_TYPE_STATS;

    $scope.isBarActive = function(bar_id){
        return $scope.bar_type == bar_id;
    };

    $scope.activateBarType = function(bt){
        $scope.bar_type = bt;
    };

    $scope.submit = function () {

    };

    $scope.filteredFields = function(){
        if(config.limit_fields == undefined){
            return $scope.player.fields;
        }
        var ret = [];
        for(var i = 0 ; i < $scope.player.fields.length ; i ++){
            if($.inArray($scope.player.fields[i].name, config.limit_fields) > -1){
                ret.push($scope.player.fields[i]);
            }
        }
        return ret;
    };

    $scope.cancel = function () {
        $modalInstance.close(true);
    };

    $scope.accurate_1p = function () {
        $scope.player.data['stats.throws_1p_accurate']++;
        $scope.player.data['stats.throws_1p_made']++;
        $scope.update();
    };

    $scope.miss_1p = function () {
        $scope.player.data['stats.throws_1p_made']++;
        $scope.update();
    };

    $scope.refreshPlayerData = function(){
        Match.refreshPlayerData($scope.player);
        DynForm.attachTo($scope, $scope.player.data);
    };

    $scope.refresh_interval = $interval(function(){
        if($scope.auto_refresh){
            $scope.refreshPlayerData();
        }
    }, 2000);

    $scope.$on('modal.closing', function(e){
        $interval.cancel($scope.refresh_interval);
    });

    $scope.attachHistoryData = function(){
        if(!$scope.attach_history_data){
            return;
        }
        for(var key in $scope.history){
            $scope.player.data['history.' + key] = $scope.history[key];
        }
    };

    $scope.show = function(){
        $scope.player.data['team.identifier'] = config.team_identifier.toUpperCase();
        if($scope.bar_type == $scope.BAR_TYPE_PERSONAL_SHOT && $scope.player.data['stats.throws_1p_made']){
            $scope.player.data['player.stat'] =  "Rzuty wolne: " + $scope.player.data['stats.throws_1p_accurate']+"/"+ $scope.player.data['stats.throws_1p_made']+" (" + $scope.player.data['virtual.throws_1p_percentage']+")";
        }
        if($scope.bar_type == $scope.BAR_TYPE_FOULS){
            $scope.player.data['player.stat'] =  "Faule: " + $scope.player.data['stats.fouls'];
        }
        if($scope.bar_type == $scope.BAR_TYPE_DATA){
            $scope.player.data['player.stat'] =  "Wzrost: " + $scope.player.data['height'] + " / Wiek: "+ $scope.player.data['age'] + " / Pozycja: "+ $scope.player.data['position'] ;
        }
        if($scope.bar_type == $scope.BAR_TYPE_POINTS){
            $scope.player.data['player.stat'] =  "Pkt. " + ($scope.player.data['stats.throws_1p_accurate']+2*$scope.player.data['stats.throws_2p_accurate']+3*$scope.player.data['stats.throws_3p_accurate']) + ", Zbiórki  " + ($scope.player.data['stats.collections_on_defence']+$scope.player.data['stats.collections_on_offence']) +  ", Bloki  " +  $scope.player.data['stats.blocks']+  ", Faule " +  $scope.player.data['stats.fouls'];
        }
        if($scope.bar_type == $scope.BAR_TYPE_POINTS2){
            $scope.player.data['player.stat'] =  "Pkt. " + ($scope.player.data['stats.throws_1p_accurate']+2*$scope.player.data['stats.throws_2p_accurate']+3*$scope.player.data['stats.throws_3p_accurate']) + ", Asysty  " + $scope.player.data['stats.assists'] +  ", Przechwyty  " +  $scope.player.data['stats.steals']+  ", Straty  " +  $scope.player.data['stats.loss'];
        }
        if($scope.player.data['stats.fouls'] > 4){
            $scope.player.data['virtual.fullname'] = "<font color=\"red\">" + $scope.player.data['virtual.fullname'] + "</font>";
        }
        $scope.attachHistoryData();
        $scope.player.data['team.name'] = $scope.team.name;
        $http({
            url: '/player_signature_bar',
            method: 'POST',
            data: $scope.player.data
        }).success(function(){
            Alert.addSuccess('Belka dla gracza ' + $scope.player.getFullName() + ' została wystawiona', 2000)
        }).error(function(data){
            console.log(data);
            Alert.addError('Błąd z serwera: ' + data.message)
        });
    };

    $scope.update = function(){
        $scope.player.data['team.identifier'] = config.team_identifier.toUpperCase()    ;
        $scope.player.data['team.name'] = $scope.team.name;
        if($scope.bar_type == $scope.BAR_TYPE_PERSONAL_SHOT && $scope.player.data['stats.throws_1p_made']){
            $scope.player.data['player.stat'] =  "Rzuty wolne: " + $scope.player.data['stats.throws_1p_accurate']+"/"+ $scope.player.data['stats.throws_1p_made']+" (" + ( $scope.player.data['stats.throws_1p_accurate'] / $scope.player.data['stats.throws_1p_made'] * 100).toFixed(1) + '%'+")";
        }
        if($scope.bar_type == $scope.BAR_TYPE_FOULS){
            $scope.player.data['player.stat'] =  "Faule: " + $scope.player.data['stats.fouls'];
        }
        $scope.attachHistoryData();
        $http({
            url: '/player_signature_bar',
            method: 'PATCH',
            data: $scope.player.data
        }).success(function(){
            Alert.addSuccess('Belka dla gracza ' + $scope.player.getFullName() + ' została zaktualizowana', 2000)
        }).error(function(data){
            console.log(data);
            Alert.addError('Błąd z serwera: ' + data.message)
        });
    };

    $scope.hide = function(){
        $http({
            url: '/player_signature_bar',
            method: 'DELETE'
        }).success(function(){
            Alert.addSuccess('Belka dla gracza ' + $scope.player.getFullName() + ' została wyczyszczona', 2000)
        }).error(function(data){
            console.log(data);
            Alert.addError('Błąd z serwera: ' + data.message)
        });
    };

    $scope.$watchCollection('data', function(){
        var recalculated_virtuals = $scope.player.recalculateVirtualFields($scope.data);
        for(var i in recalculated_virtuals){
            if($scope.data[i] == recalculated_virtuals[i]){
                continue;
            }
            $scope.data[i] = recalculated_virtuals[i];
        }

    });

});
