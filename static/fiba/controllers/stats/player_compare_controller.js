'use strict';

angular.module('fibaclient.controllers.stats.compare', [
    'ngResource'
]).controller('ComparePlayerLauncherController',  ['$scope', '$modal', function($scope, $modal) {

    $scope.openModal = function(){
        $modal.open({
            animation: false,
            templateUrl: 'fiba/templates/stats/compare.html',
            controller: 'ComparePlayersController',
            size: 'lg'
        });
    };

}])
    .controller('ComparePlayersController', ['$scope', 'Match', '$http', 'Alert',  function($scope, Match, $http, Alert) {

        $scope.Match = Match;

        $scope.teamA = Match.getTeamA();
        $scope.teamB = Match.getTeamB();

        $scope.mergeStatistics = function(){
            var obj3 = {};
            for (var attrname in $scope.players.a.stats) { obj3[attrname] = $scope.players.a.stats[attrname]; }
            for (var attrname in $scope.players.b.stats) { obj3[attrname] = $scope.players.b.stats[attrname]; }

            var total_points_a = 0;
            for(var i = 0 ; i < $scope.teamA.players.length ; i++){

                total_points_a += $scope.teamA.players[i]["stats.throws_1p_accurate"];
                total_points_a += $scope.teamA.players[i]["stats.throws_2p_accurate"] * 2;
                total_points_a += $scope.teamA.players[i]["stats.throws_3p_accurate"] * 3;
            }
            total_points_a += $scope.teamA.stats.throws_1p_accurate;
            total_points_a += $scope.teamA.stats.throws_2p_accurate * 2;
            total_points_a += $scope.teamA.stats.throws_3p_accurate * 3;

            var total_points_b = 0;
            for(var i = 0 ; i < $scope.teamB.players.length ; i++){

                total_points_b += $scope.teamB.players[i]["stats.throws_1p_accurate"];
                total_points_b += $scope.teamB.players[i]["stats.throws_2p_accurate"] * 2;
                total_points_b += $scope.teamB.players[i]["stats.throws_3p_accurate"] * 3;
            }
            total_points_b += $scope.teamB.stats.throws_1p_accurate;
            total_points_b += $scope.teamB.stats.throws_2p_accurate * 2;
            total_points_b += $scope.teamB.stats.throws_3p_accurate * 3;
            //obj3['statA.points'] = total_points_a;
            //obj3['statB.points'] = total_points_b;
            return obj3;
        };

        $scope.players = {
            "a": {
                data: {},
                stats: {}
            },
            "b": {
                data: {},
                stats: {}
            }
        };

        $scope.properties = [
            {
                'name': 'stats.name',
                'name_to_be_sent': 'name',
                'label': 'Imię i nazwisko',
                'special': function(stats){
                    return stats.first_name + ' ' + stats.last_name;
                }
            },
            {
                'name': 'stats.number',
                'name_to_be_sent': 'number',
                'label': 'Numer',
                'special': function(stats){
                    return stats.number;
                }
            },{
                'name': 'stats.points',
                'name_to_be_sent': 'points',
                'label': 'Zdobyte punkty',
                'special': function(stats){
                    return stats['stats.throws_1p_accurate'] + (2* stats['stats.throws_2p_accurate']) + (3*stats['stats.throws_3p_accurate']);
                }
            },
            {
                'name': 'stats.throws_1p_accurate',
                'name_to_be_sent': 'throws_1p',
                'label': 'rzuty osobiste',
                'special': function(stats){
                    if(stats['stats.throws_1p_made'] == 0){
                        return '0/0';
                    }else{
                        return stats['stats.throws_1p_accurate'] + '/' + stats['stats.throws_1p_made'] ;
                    }
                },
                'extra_fields': [{
                    name: 'percentage',
                    value: function(stats){
                        if(stats['stats.throws_1p_made'] == 0){
                            return '-';
                        }else{
                            return (stats['stats.throws_1p_accurate'] / stats['stats.throws_1p_made'] * 100).toFixed(1) + '%';
                        }
                    }
                }]
            },{
                'name': 'stats.throws_1p_made',
                'label': 'Wykonane rzuty wolne',
                'hidden': true
            },{
                'name': 'stats.throws_2p_accurate',
                'name_to_be_sent': 'throws_2p',
                'label': 'Rzuty za 2',
                'special': function(stats){
                    if(stats['stats.throws_2p_made'] == 0){
                        return '0/0';
                    }else{
                        return stats['stats.throws_2p_accurate'] + '/' + stats['stats.throws_2p_made'];
                    }
                },
                'extra_fields': [{
                    name: 'percentage',
                    value: function(stats){
                        if(stats['stats.throws_2p_made'] == 0){
                            return '-';
                        }else{
                            return (stats['stats.throws_2p_accurate'] / stats['stats.throws_2p_made'] * 100).toFixed(1) + '%';
                        }
                    }
                }]
            },{
                'name': 'stats.throws_2p_made',
                'label': 'Wykonane rzuty za 2',
                'hidden': true
            },{
                'name': 'stats.throws_3p_accurate',
                'name_to_be_sent': 'throws_3p',
                'label': 'Rzuty za 3',
                'special': function(stats){
                    if(stats['stats.throws_3p_made'] == 0){
                        return '0/0';
                    }else{
                        return stats['stats.throws_3p_accurate'] + '/' + stats['stats.throws_3p_made'] ;
                    }
                },
                'extra_fields': [{
                    name: 'percentage',
                    value: function(stats){
                        if(stats['stats.throws_3p_made'] == 0){
                            return '-';
                        }else{
                            return (stats['stats.throws_3p_accurate'] / stats['stats.throws_3p_made'] * 100).toFixed(1) + '%';
                        }
                    }
                }]
            },{
                'name': 'stats.throws_3p_made',
                'label': 'Wykonane rzuty za 3',
                'hidden': true
            },{
                'name': 'stats.steals',
                'name_to_be_sent': 'steals',
                'label': 'przechwyty'
            },{
                'name': 'stats.loss',
                'name_to_be_sent': 'loss',
                'label': 'straty'
            },{
                'name': 'stats.blocks',
                'name_to_be_sent': 'blocks',
                'label': 'bloki'
            },{
                'name': 'stats.fouls',
                'name_to_be_sent': 'fouls',
                'label': 'faule'
            },{
                'name': 'stats.fouls_received',
                'name_to_be_sent': 'fouls_received',
                'label': 'otrzymane faule'
            },{
                'name': 'stats.collections',
                'name_to_be_sent': 'collections_string',
                'label': 'zbiórki',
                'hidden': 'true',
                'special': function(stats) {
                    return stats['stats.collections_on_offence'] + '/' + stats['stats.collections_on_defence'];
                }
            },{
                'name': 'stats.collections_on_offence',
                'name_to_be_sent': 'collections_on_offence',
                'label': 'zbiórki w ataku'
            },{
                'name': 'stats.collections_on_defence',
                'name_to_be_sent': 'collections_on_defence',
                'label': 'zbiórki w obronie'
            },{
                'name': 'stats.assists',
                'name_to_be_sent': 'assists',
                'label': 'asysty'
            }

        ];

        $scope.getPropByName = function(name){
            for (var i = 0 ; i <  $scope.properties.length ; i++){
                if($scope.properties[i] == name){
                    return $scope.properties[i];
                }
            }
            return undefined;
        };

        $scope.show = function(){
            $http({
                url: '/player_comp',
                method: 'POST',
                data: $scope.mergeStatistics()
            }).success(function(){
                Alert.addSuccess('Porównianie zawodników zostało wygenerowane.', 2000)
            }).error(function(data){
                Alert.addError('Nie udało się wygenerować porównania:  ' + data.message)
            });
        };

        $scope.update = function(){
            $http({
                url: '/player_comp',
                method: 'PATCH',
                data: $scope.mergeStatistics()
            }).success(function(){
                Alert.addSuccess('Porównianie zawodników zostało zaktualizowane.', 2000)
            }).error(function(data){
                Alert.addError('Nie udało się wygenerować porównania:  ' + data.message)
            });
        };

        $scope.hide = function(){
            $http({
                url: '/player_comp',
                method: 'DELETE'
            }).success(function(){
                Alert.addSuccess('Porównianie zawodników zostało zdjęte.', 2000)
            }).error(function(data){
                Alert.addError('Nie udało się zdjąć porównania:  ' + data.message)
            });
        };



    }]).controller('ComparePlayersSinglePlayerController', ['$scope', '$interval', 'Match',  function($scope, $interval, Match) {

        $scope.getPlayerData = function(){
            try{
                if($scope.players[$scope.team_letter]){
                    return JSON.parse($scope.players[$scope.team_letter].data);
                }else{
                    return undefined;
                }
            }catch(e){
                return undefined;
            }
        };

        $scope.getStatValue = function(name){
            return $scope.getPlayerStats()['player' + $scope.team_letter.toUpperCase() +'.' + name];
        };

        $scope.getFullStats = function(){

        };

        $scope.getPlayerStats = function(){
            return $scope.$parent.players[$scope.team_letter].stats;
        };

        $scope.setPlayerStats = function(stats){
            $scope.$parent.players[$scope.team_letter].stats = stats;
        };

        $scope.initializeFor = function(team, team_letter){
            $scope.team_letter = team_letter;
            $scope.team = team;
        };

        $scope.recalculate_stats = function(){
            var player_data = $scope.getPlayerData();
            if(!player_data){
                return;
            }
            var stats = {};
            for(var i = 0 ; i < $scope.$parent.properties.length ; i++){
                var prop = $scope.$parent.properties[i];
                if(prop.name_to_be_sent == undefined){
                    continue;
                }
                var value = prop.special == undefined? player_data[prop.name]:prop.special(player_data, $scope.team);
                var key = 'player' + $scope.team_letter.toUpperCase() +'.' + prop.name_to_be_sent;
                stats[key] = value;
                if(prop.extra_fields == undefined){
                    continue;
                }
                for(var j = 0 ; j < prop.extra_fields.length ; j++){
                    var extra = prop.extra_fields[j];
                    stats[key + '_' + extra.name] = extra.value(player_data);
                }
            }
            $scope.setPlayerStats(stats);
        };

        $interval($scope.recalculate_stats, 1000);
        $scope.recalculate_stats();


    }]);
