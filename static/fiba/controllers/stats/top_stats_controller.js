'use strict';

angular.module('fibaclient.controllers.stats.top', [
    'ngResource'
])
    .controller('TopStatsLauncherController',  ['$scope', '$modal', function($scope, $modal) {

        $scope.openModal = function(){
            $modal.open({
                animation: false,
                templateUrl: 'fiba/templates/stats/top.html',
                controller: 'TopStatsController',
                size: 'lg'
            });
        };

    }])
    .controller('TopStatsController', ['$scope', '$modalInstance','Match', 'StatsGenerator', 'RegularStatsGenerator', 'Alert', '$http',
        function ($scope, $modalInstance, Match, StatsGenerator, RegularStatsGenerator,Alert, $http) {

            $scope.Match = Match;

            $scope.top_players_displayed = 5;

            $scope.getCurrentSortingMethodLabel = function(){
                return StatsGenerator.getSortingMethodLabelByName($scope.current_sort_by);
            };

            $scope.sort_by = StatsGenerator.stats_definition;

            $scope.current_sort_by = $scope.sort_by[0].name;
            $scope.current_ordered_statistics = {'a': [], 'b': []};

            $scope.show = function(){
                RegularStatsGenerator.setData($scope.current_ordered_statistics, $scope.current_sort_by);
                $http({
                    url: '/top5',
                    method: 'POST',
                    data: RegularStatsGenerator.getStats()
                }).success(function(){
                    Alert.addSuccess('Statystyki zostały pomyślnie wygenerowane.', 2000)
                }).error(function(data){
                    Alert.addError('Błąd z serwera: ' + data.message)
                });
            };

            $scope.update = function(){
                RegularStatsGenerator.setData($scope.current_ordered_statistics, $scope.current_sort_by);
                $http({
                    url: '/top5',
                    method: 'PATCH',
                    data: RegularStatsGenerator.getStats()
                }).success(function(){
                    Alert.addSuccess('Statystyki zostały pomyślnie odświeżone.', 2000)
                }).error(function(data){
                    Alert.addError('Błąd z serwera: ' + data.message)
                });
            };

            $scope.hide = function(){
                RegularStatsGenerator.setData($scope.current_ordered_statistics, $scope.current_sort_by);
                $http({
                    url: '/top5',
                    method: 'DELETE'
                }).success(function(){
                    Alert.addSuccess('Statystyki zostały pomyślnie zdjęte.', 2000)
                }).error(function(data){
                    Alert.addError('Błąd z serwera: ' + data.message)
                });
            };

        }]).controller('TopStatsForSingleTeamController', ['$scope', 'Match',  function ($scope,  Match) {

        $scope.team = {};

        $scope.initFor = function(team, letter){
            $scope.team = team;
            $scope.letter = letter;
            $scope.sortAndCalculateAllStatisticsForCurrentTeam();
        };

        $scope.copyStatsToRoot = function(player_data){
            if(player_data.stats == undefined){
                return;
            }
            for(var index in player_data.stats){
                player_data['stats.' + index] = player_data.stats[index];
            }
            player_data.stats = undefined;

            return player_data;
        };

        $scope.sortAndCalculateAllStatisticsForCurrentTeam = function(){
            for(var j = 0 ; j < $scope.team.players.length ; j++){
                $scope.copyStatsToRoot($scope.team.players[j]);
            }
            for(var i = 0 ; i < $scope.$parent.sort_by.length ; i++){

                $scope[$scope.$parent.sort_by[i].name_for_collection] = JSON.parse(JSON.stringify($scope.team.players))
                    .sort($scope.$parent.sort_by[i].comparator);
                $scope[$scope.$parent.sort_by[i].name_for_collection].splice($scope.top_players_displayed, 1000);
            }
        };

        $scope.$watch('$parent.current_sort_by', function(new_value){
            $scope.copyCurrentlySelectedStatisticsToParent(new_value);
        });

        $scope.copyCurrentlySelectedStatisticsToParent = function(current_statistic_name){
            for(var i = 0 ; i < $scope.$parent.sort_by.length ; i++){
                if (current_statistic_name == $scope.$parent.sort_by[i].name) {
                    $scope.$parent.current_ordered_statistics[$scope.letter] = {
                        'team': $scope.team,
                        'stats': $scope[$scope.$parent.sort_by[i].name_for_collection]
                    };
                }
            }
        };



    }]);