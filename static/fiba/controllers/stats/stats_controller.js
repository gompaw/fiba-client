'use strict';

angular.module('fibaclient.controllers.stats', [
    'ngResource'
]).controller('StatsLauncherController', ['$scope', '$modal', function($scope, $modal) {

        $scope.openModal = function(){
            $modal.open({
                animation: false,
                templateUrl: 'fiba/templates/stats/bar.html',
                controller: 'StatsController',
                size: 'lg'
            });
        };

    }])
    .controller('StatsController', ['$scope', '$modalInstance', '$interval', '$http', 'Match', 'Alert', 'HistoryForSmallStatistics',  function($scope,  $modalInstance,$interval, $http, Match, Alert, HistoryForSmallStatistics) {

        $scope.Match = Match;
        $scope.add_history = false;
        $scope.history = HistoryForSmallStatistics;

        $scope.properties = [
            {
                'name': 'stats.throws_1p_accurate',
                'label': 'rzuty wolne',
                'special': function(stats){
                    if(stats['stats.throws_1p_made'] == 0){
                        return '0/0';
                    }else{
                        return stats['stats.throws_1p_accurate'] + '/' + stats['stats.throws_1p_made'] ;
                    }
                },
                'extra_fields': [{
                    name: 'percentage',
                    value: function(stats){
                        if(stats['stats.throws_1p_made'] == 0){
                            return '-';
                        }else{
                            return (stats['stats.throws_1p_accurate'] / stats['stats.throws_1p_made'] * 100).toFixed(1) + '%';
                        }
                    }
                }]
            },{
                'name': 'stats.throws_1p_made',
                'label': 'Wykonane rzuty wolne',
                'hidden': true
            },{
                'name': 'stats.throws_2p_accurate',
                'label': 'Rzuty za 2',
                'special': function(stats){
                    if(stats['stats.throws_2p_made'] == 0){
                        return '0/0';
                    }else{
                        return stats['stats.throws_2p_accurate'] + '/' + stats['stats.throws_2p_made'];
                    }
                },
                'extra_fields': [{
                    name: 'percentage',
                    value: function(stats){
                        if(stats['stats.throws_2p_made'] == 0){
                            return '-';
                        }else{
                            return (stats['stats.throws_2p_accurate'] / stats['stats.throws_2p_made'] * 100).toFixed(1) + '%';
                        }
                    }
                }]
            },{
                'name': 'stats.throws_2p_made',
                'label': 'Wykonane rzuty za 2',
                'hidden': true
            },{
                'name': 'stats.throws_3p_accurate',
                'label': 'Rzuty za 3',
                'special': function(stats){
                    if(stats['stats.throws_3p_made'] == 0){
                        return '0/0';
                    }else{
                        return stats['stats.throws_3p_accurate'] + '/' + stats['stats.throws_3p_made'] ;
                    }
                },
                'extra_fields': [{
                    name: 'percentage',
                    value: function(stats){
                        if(stats['stats.throws_3p_made'] == 0){
                            return '-';
                        }else{
                            return (stats['stats.throws_3p_accurate'] / stats['stats.throws_3p_made'] * 100).toFixed(1) + '%';
                        }
                    }
                }]
            },{
                'name': 'stats.throws_3p_made',
                'label': 'Wykonane rzuty za 3',
                'hidden': true
            },{
                'name': 'stats.steals',
                'label': 'przechwyty'
            },{
                'name': 'stats.loss',
                'label': 'straty'
            },{
                'name': 'stats.blocks',
                'label': 'bloki'
            },{
                'name': 'stats.fouls',
                'label': 'faule'
            },{
                'name': 'stats.fouls_received',
                'label': 'otrzymane faule'
            },{
                'name': 'stats.collections',
                'label': 'zbiórki',
                'hidden': 'true',
                'special': function(stats) {
                    return stats['stats.collections_on_offence'] + '/' + stats['stats.collections_on_defence'];
                }
            },{
                'name': 'stats.collections_on_offence',
                'label': 'zbiórki w ataku'
            },{
                'name': 'stats.collections_on_defence',
                'label': 'zbiórki w obronie'
            },{
                'name': 'stats.assists',
                'label': 'asysty'
            }
        ];

        $scope.total_stats = {'A': {}, 'B': {}};

        $scope.medium_statistics = [
            {
                "label": "PUNKTY",
                "url": "/medium_stat_3"
            },
            {
                "label": "ASYSTY/PRZECHWYTY/STRATY",
                "url": "/medium_stat_1"
            },
            {
                "label": "ZBIÓRKI/BLOKI/FAULE",
                "url": "/medium_stat_2"
            }
        ];

        $scope.sendBar = function(){
            console.log($scope.total_stats);
        };

        $scope.showSmall = function(stat){



            var ret = {
                'teamA.name' : Match.getTeamA().name,
                'teamB.name' : Match.getTeamB().name,
                'teamA.shortname' : Match.getTeamA().short_name,
                'teamB.shortname' : Match.getTeamB().short_name,
                'stat.name': stat.label.toUpperCase(),

            };
            if(stat.special == undefined){
                ret['teamA.stat']  =  $scope.total_stats['A'][stat.name];
                ret['teamB.stat'] =  $scope.total_stats['B'][stat.name];
            }else{
                ret['teamA.stat']  =  stat.special($scope.total_stats['A']);
                ret['teamB.stat'] =  stat.special($scope.total_stats['B']);
            }
            if($scope.add_history){
                $scope.history.refresh_data();
                if($scope.history.team_a_data[stat.name] != undefined){
                    ret['teamA.stat_hist'] = $scope.history.team_a_data[stat.name];
                    ret['teamB.stat_hist'] = $scope.history.team_b_data[stat.name];
                }
            }


            if(stat.extra_fields != undefined){
                for(var j = 0 ; j < stat.extra_fields.length ; j ++){
                    ret['teamA.stat_' + stat.extra_fields[j].name]  =  stat.extra_fields[j].value($scope.total_stats['A']);
                    ret['teamB.stat_' + stat.extra_fields[j].name] =  stat.extra_fields[j].value($scope.total_stats['B']);
                    if($scope.add_history) {
                        if ($scope.history.team_a_data[stat.name + "_" + stat.extra_fields[j].name] != undefined) {
                            ret['teamA.stat_' + stat.extra_fields[j].name + "_hist"] = $scope.history.team_a_data[stat.name + "_" + stat.extra_fields[j].name];
                            ret['teamB.stat_' + stat.extra_fields[j].name + "_hist"] = $scope.history.team_b_data[stat.name + "_" + stat.extra_fields[j].name];
                        }
                    }
                }
            }

            var target_url = '/small_statistics';

            if($scope.add_history){
                target_url = '/small_statistics_hist';
            }

            $http({
                url: target_url,
                method: 'POST',
                data: ret
            }).success(function(){
                Alert.addSuccess('Statystyki ' + stat.label + ' zostały pomyślnie wygenerowane.', 2000)
            }).error(function(data){
                Alert.addError('Nie udało się wygenerować statystyk:  ' + data.message)
            });

        };

        $scope.hideSmall = function(){
            $http({
                url: '/small_statistics',
                method: 'DELETE'
            }).success(function(){
                Alert.addSuccess('Schowano małą belkę.', 500)
            }).error(function(data){
                Alert.addError('Nie udało się schować małej belki!');
            });
        };

        $scope.generateBigStatistics = function(){
            function generateStatsFor(element, team){
                element['stat' + team + '.assists'] = $scope.total_stats[team]['stats.assists'];
                element['stat' + team + '.blocks'] = $scope.total_stats[team]['stats.blocks'];
                element['stat' + team + '.points'] = $scope.total_stats[team]['stats.throws_1p_accurate']+$scope.total_stats[team]['stats.throws_2p_accurate']*2+$scope.total_stats[team]['stats.throws_3p_accurate']*3;
                element['stat' + team + '.collections'] = $scope.total_stats[team]['stats.collections_on_defence']+$scope.total_stats[team]['stats.collections_on_offence'];
                element['stat' + team + '.collections_on_defence'] = $scope.total_stats[team]['stats.collections_on_defence'];
                element['stat' + team + '.collections_on_offence'] = $scope.total_stats[team]['stats.collections_on_offence'];
                element['stat' + team + '.collections_string'] = $scope.total_stats[team]['stats.collections_on_offence'] + '/' + $scope.total_stats[team]['stats.collections_on_defence'];
                element['stat' + team + '.fouls'] = $scope.total_stats[team]['stats.fouls'];
                element['stat' + team + '.steals'] = $scope.total_stats[team]['stats.steals'];
                element['stat' + team + '.loss'] = $scope.total_stats[team]['stats.loss'];
                element['stat' + team + '.fouls_received'] = $scope.total_stats[team]['stats.fouls_received'];


                var points = ['1', '2', '3'];
                for(var i = 0 ; i < points.length ; i ++){
                    var num = points[i];
                    element['stat' + team + '.throws_' + num + 'p'] = $scope.total_stats[team]['stats.throws_' + num + 'p_accurate'] + "/" + $scope.total_stats[team]['stats.throws_' + num + 'p_made'] ;
                    if($scope.total_stats[team]['stats.throws_' + num + 'p_made'] == 0){
                        element['stat' + team + '.throws_' + num + 'p_percentage'] = '-';
                    }else{
                        element['stat' + team + '.throws_' + num + 'p_percentage'] =  ( $scope.total_stats[team]['stats.throws_' + num + 'p_accurate'] /  $scope.total_stats[team]['stats.throws_' + num + 'p_made'] * 100).toFixed(1) +"%";
                    }
                }
                if($scope.add_history){
                    $scope.history.refresh_data();
                    for (var key in element) {
                        if($scope.history.team_a_data_big[key] != undefined){
                            element[key+"_hist"] = $scope.history.team_a_data_big[key];
                        } else if($scope.history.team_b_data_big[key] != undefined){
                            element[key+"_hist"] = $scope.history.team_b_data_big[key];
                        }
                    }
                }
            }

            var ret = {
                'teamA.name' : Match.getTeamA().name,
                'teamB.name' : Match.getTeamB().name,
                'teamA.shortname' : Match.getTeamA().short_name,
                'teamB.shortname' : Match.getTeamB().short_name
            };

            generateStatsFor(ret, 'A');
            generateStatsFor(ret, 'B');
            return ret;
        };

        $scope.showBig = function(controller_url){


            if(controller_url == undefined){
                controller_url = '/huge_statistics';
            }


            var ret = $scope.generateBigStatistics();

            if($scope.add_history){
                controller_url += "_hist";
            }

            $http({
                url: controller_url,
                method: 'POST',
                data: ret
            }).success(function(){
                Alert.addSuccess('Statystyki zostały pomyślnie wygenerowane.', 2000)
            }).error(function(data){
                Alert.addError('Nie udało się wygenerować statystyk:  ' + data.message)
            });
        };

        $scope.updateBig = function(){

            var ret = $scope.generateBigStatistics();

            $http({
                url: '/huge_statistics',
                method: 'PATCH',
                data: ret
            }).success(function(){
                Alert.addSuccess('Statystyki zostały pomyślnie zaktualizowane.', 2000)
            }).error(function(data){
                Alert.addError('Nie udało się zaktualizować statystyk:  ' + data.message)
            });
        };

        $scope.hideBig = function(){

            $http({
                url: '/huge_statistics',
                method: 'DELETE'
            }).success(function(){
                Alert.addSuccess('Statystyki zostały pomyślnie zdjęte.', 2000)
            }).error(function(data){
                Alert.addError('Nie udało się zdjąć statystyk:  ' + data.message)
            });
        };

    }]);
