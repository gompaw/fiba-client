'use strict';

angular.module('fibaclient.controllers.stats.team', [
    'ngResource'
])
    .controller('TeamStatsController', ['$scope', 'Match',  function($scope, Match) {

        $scope.team = {};
        $scope.team_letter = '';
        $scope.stats = {};


        $scope.fields_stackable_with_whole_team = ['stats.fouls', 'stats.steals', 'stats.collections_on_defence', 'stats.collections_on_offence'];

        $scope.initFor = function(team){
            $scope.team = team=='A'?Match.getTeamA():Match.getTeamB();
            $scope.team_letter = team;
            for(var i = 0 ; i < $scope.properties.length ; i ++){
                $scope.stats[$scope.properties[i].name] = $scope.sumPlayersProperty($scope.properties[i].name);

            }
            $scope.$parent.total_stats[team] = $scope.stats;
        };

        $scope.copyStatsToRoot = function(player_data){
            if(player_data.stats == undefined){
                return;
            }
            for(var index in player_data.stats){
                player_data['stats.' + index] = player_data.stats[index];
            }
            player_data.stats = undefined;

            return player_data;
        };

        $scope.sumPlayersProperty = function(property){
            var sum = 0;
             $scope.copyStatsToRoot($scope.team);
            for(var i = 0 ; i < $scope.team.players.length ; i ++){
                $scope.copyStatsToRoot($scope.team.players[i]);
                sum += $scope.team.players[i][property];
            }

            if($scope.fields_stackable_with_whole_team.indexOf(property) > -1){
                sum += $scope.team[property];
            }

            return sum;
        };



    }]);