'use strict';

angular.module('fibaclient.controllers.pitch', [
    'ngResource'
])
    .controller('PitchController', ['$scope', '$interval', 'config', '$http', 'Alert', function($scope, $interval, config, $http, Alert) {

        $scope.team = config.team;
        $scope.team_letter = config.team_letter;

        $scope.getPlayerByNumber = function(player){
            var num_int = parseInt(player.text);
            for(var i = 0 ; i < $scope.team.players.length ; i ++){
                if($scope.team.players[i].number == num_int){
                    player.player.name = $scope.team.players[i].first_name;
                    player.player.surname = $scope.team.players[i].last_name;
                    return $scope.team.players[i].first_name + ' ' + $scope.team.players[i].last_name;
                }
            }
            return ' ';
        };

        $scope.getCurrentTeamStorage = function(){
            return "team_" + $scope.team_letter + "_squad";
        };

        $scope.$watch('positions', function(new_value, old_value){
            localStorage.setItem($scope.getCurrentTeamStorage(), JSON.stringify(new_value));
        },1);

        $scope.empty_positions = [
            {
                top: 64,
                left: 44.5,
                text: '',
                id: 'D',
                player: {
                    name: '',
                    surname: ''
                }
            },
            {
                top: 11,
                left: 61,
                text: '',
                id: 'B',
                player: {
                    name: '',
                    surname: ''
                }
            },
            {
                top: 11,
                left: 28,
                text: '',
                id: 'A',
                player: {
                    name: '',
                    surname: ''
                }
            },
            {
                top: 50,
                left: 11.5,
                text: '',
                id: 'C',
                player: {
                    name: '',
                    surname: ''
                }
            },
            {
                top: 50,
                left: 78,
                text: '',
                id: 'E',
                player: {
                    name: '',
                    surname: ''
                }
            }
        ];

        if(localStorage.getItem($scope.getCurrentTeamStorage())){
            $scope.positions = JSON.parse(localStorage.getItem($scope.getCurrentTeamStorage()));
        }else{
            $scope.positions = $scope.empty_positions;
        }

        $scope.show = function(){
            var ret = {
                'team.name': $scope.team.name,
                'logo' : $scope.team_letter.toUpperCase()
            };
            for(var i = 0 ; i < $scope.positions.length ; i ++){
                var letter = $scope.positions[i].id;
                ret['player' + letter + '.surname'] = $scope.positions[i].player.surname;
                ret['player' + letter + '.firstletter'] = $scope.positions[i].player.name[0];
                ret['player' + letter + '.number'] = parseInt($scope.positions[i].text);
            }
            $http({
                url: '/first5',
                method: 'POST',
                data: ret
            }).success(function(){
                Alert.addSuccess('Ustawienie "' + $scope.team.short_name + '" zostało wystawione', 2000)
            }).error(function(data){
                Alert.addError('Błąd z serwera: ' + data.message)
            });
        };

        $scope.update = function(){
            var ret = {
                'team.name': $scope.team.name
            };
            for(var i = 0 ; i < $scope.positions.length ; i ++){
                var letter = $scope.positions[i].id;
                ret['player' + letter + '.surname'] = $scope.positions[i].player.surname;
                ret['player' + letter + '.firstletter'] = $scope.positions[i].player.name[0];
                ret['player' + letter + '.number'] = parseInt($scope.positions[i].text);
            }
            $http({
                url: '/first5',
                method: 'PATCH',
                data: ret
            }).success(function(){
                Alert.addSuccess('Ustawienie "' + $scope.team.short_name + '" zostało wystawione', 2000)
            }).error(function(data){
                Alert.addError('Błąd z serwera: ' + data.message)
            });
        };

        $scope.hide = function(){
            $http({
                url: '/first5',
                method: 'DELETE'
            }).success(function(){
                Alert.addSuccess('Ustawienie "' + $scope.team.short_name + '" zostało schowane', 2000)
            }).error(function(data){
                Alert.addError('Błąd z serwera: ' + data.message)
            });
        };

    }]).controller('PitchLauncherController', ['$scope', '$interval', '$modal', function($scope, $interval, $modal) {


        $scope.openModal = function(){
            var modalInstance = $modal.open({
                animation: false,
                templateUrl: 'fiba/templates/pitch/bars/pitch.html',
                controller: 'PitchController',
                size: 'md',
                resolve: {
                    config: function () {
                        return {
                            team: $scope.team,
                            team_letter: $scope.team_identificator
                        };
                    }
                }
            });
        };

    }]);