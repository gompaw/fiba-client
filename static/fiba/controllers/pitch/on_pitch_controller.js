'use strict';

angular.module('fibaclient.controllers.on_pitch', [
    'ngResource'
]).controller('OnPitchLauncherController', ['$scope', '$modal', 'UpdateOnPitchSignal', function($scope, $modal, UpdateSquadSignal) {

    $scope.openModal = function(){
        if($scope.team_identificator !== 'a' && $scope.team_identificator !== 'b'  ){
            return;
        }
        $modal.open({
            animation: false,
            templateUrl: 'fiba/templates/pitch/bars/on_pitch.html',
            controller: 'OnPitchController',
            size: 'lg',
            resolve: {
                team: function () {
                    return $scope.team_identificator;
                }
            }
        });
    };



}])
    .controller('OnPitchController', ['$scope', '$modalInstance', '$interval', 'Match', 'UpdateOnPitchSignal', 'team',
        function($scope,  $modalInstance,$interval, Match,UpdateOnPitchSignal, team) {

            $scope.refreshTeam = function(){
                if( $scope.auto_refresh){
                    $scope.team = team=='a'?Match.getTeamA():Match.getTeamB();
                }
                if(team=='a'){
                    UpdateOnPitchSignal.setTeamA($scope.team);
                }else{
                    UpdateOnPitchSignal.setTeamB($scope.team);
                }
            };

            $scope.auto_refresh = true;
            $scope.refreshTeam();

            $interval($scope.refreshTeam, 200);

            $scope.show = function(){
                UpdateOnPitchSignal.show(team);
            };

            $scope.update = function(){
                UpdateOnPitchSignal.update(team);
            };

            $scope.hide = function(){
                UpdateOnPitchSignal.hide();
            }
        }]);