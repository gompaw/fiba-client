'use strict';

angular.module('fibaclient.controllers.mode', [
    'ngResource'
])
    .controller('ModeController', ['$scope', '$interval', 'Score', function($scope, $interval, Score) {

        $scope.Score = Score;

        $scope.current_mode = Score.useManualScoring();

        $scope.$watch('current_mode', function(new_value){
            Score.setScoringMode(new_value);
        })

        $scope.sync_mode = Score.useManualSync();

        $scope.$watch('sync_mode', function(new_value){
            Score.setSyncMode(new_value);
        })
    }])

