'use strict';

angular.module('fibaclient.controllers.main', [])
    .controller('MainController', [
        '$scope', '$interval', 'Match', 'Alert', 'Config',
        function($scope, $interval, Match, Alert,Config) {

        Match.startDownloader({
            success: function(){
                $scope.updateMatchIfNewDataDiffersFromCurrent($scope.team_a,  Match.getTeamA(), 'a');
                $scope.updateMatchIfNewDataDiffersFromCurrent($scope.team_b,  Match.getTeamB(), 'b');
            },
            failure: function(response){
                Alert.addError(response.data.message, 2000);
            }
        });

        $scope.updateMatchIfNewDataDiffersFromCurrent = function(match, new_data, letter){
            if(JSON.stringify(match) == JSON.stringify(new_data)){
                return;
            }
            match = new_data;
            $scope.$broadcast('team:changed', letter, match);
        };


        $scope.alert = Alert;

        $scope.deleteAlert = function(id){
            Alert.close(id);
        };

        $scope.getBootstrapPath = function(){
            return Config.getValue('theme');
        };





    }]);