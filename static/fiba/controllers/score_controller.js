'use strict';

angular.module('fibaclient.controllers.score', [
    'ngResource'
])
    .controller('ScoreController', ['$scope', '$interval', '$timeout', 'Score', function($scope, $interval, $timeout, Score) {

        $scope.Score = Score;

        $scope.current_score_data = {};

        $scope.$watch('team_identificatior', function(){
            if($scope.team_identificator == 'a' || $scope.team_identificator == 'b'){
                $scope.updateScore = $scope.team_identificator=='a'?Score.getTeamA:Score.getTeamB;
                $scope.getScoreSum = $scope.team_identificator=='a'?Score.getTotalScoreForTeamA:Score.getTotalScoreForTeamB;
                $scope.addToScore = $scope.team_identificator=='a'?Score.addPointsToA:Score.addPointsToB;
            }
        });

        $interval(function(){
            if($scope.updateScore !== undefined){
                $timeout(function(){
                    $scope.current_score_data = $scope.updateScore();
                });
            }
        }, Score.getTimeout());

    }]);