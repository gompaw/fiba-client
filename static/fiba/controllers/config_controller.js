'use strict';

angular.module('fibaclient.controllers.config', [])
    .controller('ConfigController', ['$scope', '$rootScope', 'Config', function($scope, $rootScope, Config) {

        $scope.show_config = false;
        $scope.data = {};

        $scope.toggleConfigArea = function(){
            $scope.show_config = !$scope.show_config;
            if($scope.show_config){
                $scope.onConfigLaunched();
            }else{
                $scope.onConfigClosed();
            }
        };

        $scope.onConfigLaunched = function(){


        };

        $scope.onConfigClosed = function(){

        };

        $scope.cloneConfigEntriesToData = function(){
            for(var i = 0 ; i < $scope.config_entries.length ; i++){
                $scope.data[$scope.config_entries[i].name] = Config.getValue($scope.config_entries[i].name)
            }
        };

        $scope.$watchCollection('data', function(){
            for(var key in $scope.data){
                Config.setValue(key, $scope.data[key]);
            }
        });

        $scope.config_entries = [
            {
                "label": "Skórka",
                "name": "theme",
                "type": "select",
                "options":{
                    "bower_components/bootstrap/dist/css/bootstrap.min.css": "Bootstrap",
                    "fiba/styles/themes/cosmo.css": "Cosmo",
                    "fiba/styles/themes/lumen.css": "Lumen",
                    "fiba/styles/themes/paper.css": "Paper",
                    "fiba/styles/themes/yeti.css": "Yeti"
                }
            }
        ];

        $scope.isText = function(field){
            return field.type == "text";
        };

        $scope.isNumber = function(field){
            return field.type == "number";
        };

        $scope.isSelect = function(field){
            return field.type == "select";
        };

        $scope.isBool = function(field){
            return field.type == "bool";
        };

        $scope.cloneConfigEntriesToData();

    }]);