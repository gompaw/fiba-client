'use strict';

angular.module('fibaclient.controllers.scorebox', [
    'ngResource'
])
    .controller('ScoreboxController', ['$scope', '$interval', '$timeout', 'Score', function($scope, $interval, $timeout, Score) {

        $scope.Score = Score;
        $scope.selected_description = ' ';
        $scope.description_options = Score.getDescriptionOptions();


        $scope.$watch('selected_description', function(new_value){
            Score.setDescription(new_value);
        });

        $scope.show = function(){
            Score.showScorebox();
        };

        $scope.hide = function(){
            Score.hideScorebox();
        };

    }]);