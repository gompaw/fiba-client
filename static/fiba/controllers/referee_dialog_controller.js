'use strict';

angular.module('fibaclient.controllers.referee', [
    'ngResource'
])
    .controller('RefereeButtonController', ['$scope', '$modal', function($scope, $modal) {

        $scope.openModal = function(){
            var modalInstance = $modal.open({
                animation: false,
                templateUrl: 'fiba/templates/referee/dialog.html',
                controller: 'RefereeDialogController',
                size: 'large',
                resolve: {
                    config: function () {
                        return {
                        };
                    }
                }
            });

            modalInstance.result.then(function (selectedItem) {

            }, function () {

            });
        };


    }]).
    controller('RefereeDialogController', function ($scope,  Match,  $modalInstance, $http, Alert,  config) {

        var self = this;

        $scope.copyReferees = function(){
            var all_referees = Match.getReferees();
            $scope.referee_1 = all_referees.referee_1;
            $scope.referee_2 = all_referees.referee_2;
            $scope.referee_3 = all_referees.referee_3;
            $scope.commissioner = all_referees.commissioner;
        };

        $scope.submit = function () {
            $modalInstance.close(true);
        };

        $scope.cancel = function () {
            $modalInstance.dismiss(false);
        };

        $scope.copyReferees();

        $scope.show = function(){
            $http({
                url: '/referee_signature_bar',
                method: 'POST',
                data: {
                    "referee1.name": $scope.referee_1,
                    "referee2.name": $scope.referee_2,
                    "referee3.name": $scope.referee_3,
                    "commissioner.name": $scope.commissioner
                }
            }).success(function(){
                Alert.addSuccess('Sędziowie zostali podpisani.', 2000)
            }).error(function(data){
                Alert.addError('Nie udało się podpisać sędziów:  ' + data.message, 2000)
            });
        }

        $scope.update = function(){
            $http({
                url: '/referee_signature_bar',
                method: 'PATCH',
                data: {
                    "referee1.name": $scope.referee_1,
                    "referee2.name": $scope.referee_2,
                    "referee3.name": $scope.referee_3,
                    "commissioner.name": $scope.commissioner,
                }
            }).success(function(){
                Alert.addSuccess('Sędziowie zostali zaktualizowani.', 2000)
            }).error(function(data){
                Alert.addError('Nie udało się podpisać sędziów:  ' + data.message, 2000)
            });
        }

        $scope.hide = function(){
            $http({
                url: '/referee_signature_bar',
                method: 'DELETE'
            }).success(function(){
                Alert.addSuccess('Sędziowie zostali zdjęci.', 2000)
            }).error(function(data){
                Alert.addError('Nie udało się zdjąć sędziów:  ' + data.message, 2000)
            });
        }



    });