'use strict';

angular.module('fibaclient.controllers.combo', [
    'ngResource'
])
    .controller('ComboDescriptionController', ['$scope', '$interval', '$timeout', 'Score', function($scope, $interval, $timeout, Score) {

        $scope.Score = Score;
        $scope.are_fouls_active = true;


        $scope.show = function(){
            Score.showCombo();
        };

        $scope.hide = function(){
            Score.hideCombo();
        };

        $scope.hideFouls = function(){
            $scope.are_fouls_active = false;
            Score.hideFouls();
        };

        $interval(function(){
            if($scope.are_fouls_active){
                Score.showFouls();
            }
        }, 1000);

    }]);