'use strict';

angular.module('fibaclient.controllers.team', [
    'ngResource'
])
    .controller('TeamController', ['$scope', function($scope) {

        // a or b
        $scope.team_identificator = "";

        $scope.team = {};


        $scope.$on('team:changed', function(e, type, data){
            if($scope.team_identificator == type){
                 $scope.team = data;
                $scope.team_model = new Team($scope.team);
            }
        });

        $scope.$on('team:swap_requested', function(e){
            $scope.team_identificator = $scope.team_identificator=="a"?"b":"a";
        });

        $scope.launchFor = function(team_identificator){
            $scope.team_identificator = team_identificator;
        };


    }]);