'use strict';

angular.module('fibaclient.controllers.team.bars.squad', [
    'ngResource'
]).controller('TeamSquadBarLauncherController', ['$scope', '$modal', 'UpdateSquadSignal', function($scope, $modal, UpdateSquadSignal) {

    $scope.openModal = function(){
        if($scope.team_identificator !== 'a' && $scope.team_identificator !== 'b'  ){
            return;
        }
        $modal.open({
            animation: false,
            templateUrl: 'fiba/templates/team/bars/team_squad.html',
            controller: 'TeamSquadBarController',
            size: 'lg',
            resolve: {
                team: function () {
                    return $scope.team_identificator;
                }
            }
        });
    };



}])
    .controller('TeamSquadBarController', ['$scope', '$modalInstance', '$interval', 'Match', 'UpdateSquadSignal', 'team',
        function($scope,  $modalInstance,$interval, Match,UpdateSquadSignal, team) {

            $scope.refreshTeam = function(){
                if( $scope.auto_refresh){
                    $scope.team = team=='a'?Match.getTeamA():Match.getTeamB();
                }
                if(team=='a'){
                    UpdateSquadSignal.setTeamA($scope.team);
                }else{
                    UpdateSquadSignal.setTeamB($scope.team);
                }
            };

            $scope.auto_refresh = true;
            $scope.refreshTeam();

            $interval($scope.refreshTeam, 200);

            $scope.show = function(){
                UpdateSquadSignal.show(team);
            };

            $scope.update = function(){
                UpdateSquadSignal.update(team);
            };

            $scope.hide = function(){
                UpdateSquadSignal.hide();
            }
        }]);