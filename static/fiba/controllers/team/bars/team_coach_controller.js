'use strict';

angular.module('fibaclient.controllers.team.bars.coach', [
    'ngResource'
])
    .controller('TeamCoachBarController', ['$scope', '$http', 'config', 'Alert', function($scope, $http, config, Alert) {

        $scope.team = config.team;

        console.log(config.identifier);

        $scope.show = function(){
            $http({
                url: '/coach_signature_bar',
                method: 'POST',
                data: {
                    coach_name: $scope.team.coach,
                    coach_team: $scope.team.name,
                    logo: config.identifier.toUpperCase()
                }
            }).success(function(){
                Alert.addSuccess('Trener ' + $scope.team.name + ' został podpisany.', 2000)
            }).error(function(data){
                Alert.addError('Nie udało się podpisać trenera:  ' + data.message, 2000)
            });
        };

         $scope.update = function(){
            $http({
                url: '/coach_signature_bar',
                method: 'PATCH',
                data: {
                    name: $scope.team.coach,
                    team_name: $scope.team.name
                }
            }).success(function(){
                Alert.addSuccess('Trener ' + $scope.team.name + ' został zaktualizowany.', 2000)
            }).error(function(data){
                Alert.addError('Nie udało się podpisać trenera:  ' + data.message, 2000)
            });
        }

         $scope.hide = function(){
            $http({
                url: '/coach_signature_bar',
                method: 'DELETE'
            }).success(function(){
                Alert.addSuccess('Trener ' + $scope.team.name + ' został zdjęty.', 2000)
            }).error(function(data){
                Alert.addError('Nie udało się podpisać trenera:  ' + data.message, 2000)
            });
        }

    }]).controller('TeamCoachBarLauncherController', ['$scope', '$modal', function($scope, $modal) {

        $scope.openModal = function(){
            var modalInstance = $modal.open({
                animation: false,
                templateUrl: 'fiba/templates/team/bars/team_coach.html',
                controller: 'TeamCoachBarController',
                size: 'lg',
                resolve: {
                    config: function () {
                        return {
                            team: $scope.team,
                            identifier: $scope.team_identificator
                        };
                    }
                }
            });

            modalInstance.result.then(function (selectedItem) {

            }, function () {

            });
        };


    }]);