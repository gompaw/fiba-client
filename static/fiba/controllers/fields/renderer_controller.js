'use strict';

angular.module('fibaclient.controllers.fields.renderer', [

])
    .controller('RendererFieldController', ['$scope', function($scope) {

        $scope.current_field = {};

        $scope.initFor = function(field){
            $scope.current_field = field;
        };

        $scope.isFieldText = function(field){
            return $scope.current_field.type == "text";
        };

        $scope.isFieldNumber = function(field){
            return $scope.current_field.type == "number";
        };

        $scope.isFieldSelect = function(field){
            return $scope.current_field.type == "select";
        };

        $scope.isFieldBool = function(field){
            return $scope.current_field.type == "bool";
        };



    }]);