'use strict';

angular.module('fibaclient.controllers.fields.number', [

])
    .controller('NumberFieldController', ['$scope', function($scope) {

        $scope.field_definition = {};
        $scope.model = {};
        $scope.step = 1;

        $scope.initFor = function(config){
            $scope.field_definition = config.field;
            if(config.step != undefined){
                $scope.step = config.step;
            }
            $scope.model = config.model;
        };

        $scope.incrementValue = function(value){
            if(value == null || isNaN(value)){
                value = 0;
            }

            var hlp = parseFloat(value);
            return  hlp + $scope.step;
        };

        $scope.decrementValue = function(value){
            if(value == null || isNaN(value)){
                value = 0;
            }

            var hlp = parseFloat(value);
            return  hlp - $scope.step;
        };

    }]);