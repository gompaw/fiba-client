function Team(team){

    var self = this;

    self.data = team;

    self.getPlayerIndexById = function(id){
        for(var i = 0 ; i < self.data.players.length ; i ++){
            if(self.data.players.id == id){
                return i;
            }
        }
        return undefined;
    };

    self.getPlayerById = function(id){
        var player_index = self.getPlayerIndexById(id);
        if(player_index == undefined){
            return undefined;
        }
        return self.data.players(player_index);
    };

    self.attachUniqueIdTo = function(player){
        player.id = player.number + parseInt(Math.random()*10000);
        return self.getPlayerById(player.id)?self.attachUniqueIdTo(player.id):player;
    };

    self.addPlayer = function(player){
        self.data.players.push(self.attachUniqueIdTo(player));
    }
};