'use strict';

angular.module('fibaclient.models.match',[])
    .factory('Match',
    ['$resource', '$interval', '$timeout', 'Alert',
        function($resource, $interval,$timeout, Alert) {

            var self = this;
            self.data = {};
            self.errors = [];

            self.modificators = {
                'a' : {
                    1: 4,
                    2: 0,
                    3: 0,
                    4: 0
                },
                'b' : {
                    1: 0,
                    2: 0,
                    3: 0,
                    4: 0
                }
            };

            self.modifyResult = function(team_letter, modification){
                var current_quart = self.getCurrentQuart();
                self.modificators[team_letter][current_quart] = modification;
            };

            self.copyStatsToRoot = function(player_data){
                if(player_data.stats == undefined){
                    return;
                }
                for(var index in player_data.stats){
                    player_data['stats.' + index] = player_data.stats[index];
                }

                return player_data;
            };

            self.copyWholeTeamsStatsToRoot = function(team){
                self.copyStatsToRoot(team);
                for(var i = 0 ; i < team.players.length; i++){
                    self.copyStatsToRoot(team.players[i]);
                }
            };

            self.repeatedlyRefreshMatchInfo = function(config){
                $interval(function(){
                    self.downloadMatchInfo({
                        failure: function(data, status_code){
                            self.errors.push(data);
                            config.failure?config.failure(data, status_code):undefined;
                        },
                        success: function(data, status){
                            self.copyStatsToRoot(data.teamA);
                            self.copyStatsToRoot(data.teamB);
                            self.data = data;
                            config.success?config.success(data, status):undefined;
                        }
                    });
                }, 2000);

                self.data = self.downloadMatchInfo({
                    failure: function(data, status_code){
                        self.errors.push(data);
                        config.failure?config.failure(data, status_code):undefined;
                    },
                    success: config.success
                });

            };

            self.getCurrentQuart = function(){
                if($scope.data.time == 0){
                    return 1;
                }else{
                    return parseInt(4 * $scope.data.time / 90);
                }
            };

            self.downloadMatchInfo = function(config){
                return $resource('/match', {},  {'query':  {method:'GET', isArray:false}}).query({},{}, config&&config.success?config.success:undefined, config&&config.failure?config.failure:undefined);
            };

            self.refreshPlayerData = function(player){
                var team = undefined;
                if(self.data.teamA.name == player.team.name){
                    team = self.data.teamA;
                }else if(self.data.teamB.name == player.team.name){
                    team = self.data.teamB;
                }else{
                    Alert.addError( "Nie ma takiej drużyny jak" + JSON.stringify(player.team));
                    return;
                }

                for(var i = 0 ; i < team.players.length ; i++){
                    if(team.players[i].number == player.data.number){
                        $timeout(function(){
                            player.data = team.players[i];
                        });
                        return;
                    }
                }
                Alert.addError("W drużynie " + team.name + " nie ma zawodnika o numerze " + player.data.number, 3000);
            };

            return {
                'hasErrors': function(){
                    return self.errors.length > 0;
                },
                'getError': function(){
                    return self.errors.pop();
                },
                'isReady': function(){
                    return self.data !== {};
                },
                'getTeamA': function(){
                    return self.data.teamA;
                },
                'getTeamB': function(){
                    return self.data.teamB;
                },
                'startDownloader': function(config){
                    self.repeatedlyRefreshMatchInfo(config);
                },
                'getReferees': function(){
                    return {
                        "referee_1": self.data.referee_1,
                        "referee_2": self.data.referee_2,
                        "referee_3": self.data.referee_3,
                        "commissioner": self.data.commissioner
                    }
                },
                'refreshPlayerData': self.refreshPlayerData
            }
        }]);