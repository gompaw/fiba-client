﻿function Player(player){

    var self = this;

    self.getFullName = function(){
        return self.data.first_name + ' ' + self.data.last_name;
    };

    self.fields = [
        {
            "name": 'first_name',
            "label": "Imię",
            "type": "text"
        },
        {
            "name": 'last_name',
            "label": "Nazwisko",
            "type": "text"
        },
        {
            "name": 'age',
            "label": "Wiek",
            "type": "number"
        },
        {
            "name": 'height',
            "label": "Wysokość",
            "type": "number"
        },
        {
            "name": 'number',
            "label": "Numer na koszulce",
            "type": "number"
        },
        {
            "name": 'stats.assists',
            "label": "Asysty",
            "type": "number"
        },
        {
            "name": 'stats.collections_on_defence',
            "label": "Zbiórki w obronie",
            "type": "number"
        },
        {
            "name": 'stats.collections_on_offence',
            "label": "Zbiórki w ataku",
            "type": "number"
        },
        {
            "name": 'stats.fouls',
            "label": "Faule",
            "type": "number"
        },
        {
            "name": 'stats.fouls_received',
            "label": "Faule na zawodniku",
            "type": "number"
        },
        {
            "name": 'stats.last_sec_on_field',
            "label": "Ostatnio na boisku",
            "type": "number"
        },
        {
            "name": 'stats.loss',
            "label": "Straty",
            "type": "number"
        },
        {
            "name": 'stats.new_points_while_on_court',
            "label": "Nety",
            "type": "number"
        },
        {
            "name": 'stats.steals',
            "label": "Przechwyty",
            "type": "number"
        },
        {
            "name": 'stats.throws_1p_accurate',
            "label": "Celne rzuty wolne",
            "type": "number"
        },
        {
            "name": 'stats.throws_2p_accurate',
            "label": "Celne rzuty za 2",
            "type": "number"
        },
        {
            "name": 'stats.throws_3p_accurate',
            "label": "Celne rzuty za 3",
            "type": "number"
        },
        {
            "name": 'stats.throws_1p_made',
            "label": "Wykonane rzuty wolne",
            "type": "number"
        },
        {
            "name": 'stats.throws_2p_made',
            "label": "Wykonane rzuty za 2",
            "type": "number"
        },
        {
            "name": 'stats.throws_3p_made',
            "label": "Wykonane rzuty za 3",
            "type": "number"
        },
        {
            "name": 'stats.blocks',
            "label": "Bloki",
            "type": "number"
        },
        {
            "name": 'virtual.firstletter',
            "label": "Pierwsza litera imienia",
            "type": "text"
        },
        {
            "name": 'virtual.throws_1p_percentage',
            "label": "% celnych rzutow wolnych",
            "type": "text"
        },
        {
            "name": 'virtual.throws_2p_percentage',
            "label": "% celnych rzutow za 2",
            "type": "text"
        },
        {
            "name": 'virtual.throws_3p_percentage',
            "label": "% celnych rzutow za 3",
            "type": "text"
        },
        {
            "name": 'virtual.in_field',
            "label": "Na boisku?",
            "type": "bool"
        }


    ];

    self.virtual_fields = [
        {
            'name': 'firstletter',
            'value': function(player_data){
                return player_data.first_name[0];
            }
        },
        {
            'name': 'fullname',
            'value': function(player_data){
                if ((player_data.first_name + ' '+ player_data.last_name).length > 22){
                    return player_data.first_name[0] + '. '+ player_data.last_name;
                }
                return player_data.first_name + ' '+ player_data.last_name;
            }
        },
        {
            'name': 'throws_1p_percentage',
            'depends_on': ['stats.throws_1p_made', 'stats.throws_1p_accurate'],
            'value': function(player_data){
                if(player_data['stats.throws_1p_made'] == 0){
                    return '';
                }
                return  (player_data['stats.throws_1p_accurate'] / player_data['stats.throws_1p_made'] * 100).toFixed(1) + '%';
            }
        },
        {
            'name': 'throws_2p_percentage',
            'depends_on': ['stats.throws_2p_made', 'stats.throws_2p_accurate'],
            'value': function(player_data){
                if(player_data['stats.throws_2p_made'] == 0){
                    return '';
                }
                return  (player_data['stats.throws_2p_accurate'] / player_data['stats.throws_2p_made'] * 100).toFixed(1) + '%';
            }
        },
        {
            'name': 'throws_3p_percentage',
            'depends_on': ['stats.throws_3p_made', 'stats.throws_3p_accurate'],
            'value': function(player_data){
                if(player_data['stats.throws_3p_made'] == 0){
                    return '';
                }
                return  (player_data['stats.throws_3p_accurate'] / player_data['stats.throws_3p_made'] * 100).toFixed(1) + '%';
            }
        },
        {
            'name': 'in_field',
            'value': function(player_data){
                return player_data['in_field'];
            }
        },


    ];

    self.getPlayerProperty = function(name){
        return self.data.data[name];
    };

    self.isOnPitch = function(){
        return self.data['in_field'];
    };


    self.copyStatsToRoot = function(player_data){
        if(player_data.stats == undefined){
            return;
        }
        for(var index in player_data.stats){
            player_data['stats.' + index] = player_data.stats[index];
        }
        player_data.stats = undefined;
        for(var i = 0; i < self.virtual_fields.length ; i ++){
            player_data['virtual.' + self.virtual_fields[i].name] = self.virtual_fields[i].value(player_data);
        }
        return player_data;
    };

    self.recalculateVirtualFields = function(old_data){
        var player_data = {};
        for(var i = 0; i < self.virtual_fields.length ; i ++){
            player_data['virtual.' + self.virtual_fields[i].name] = self.virtual_fields[i].value(old_data);
        }
        return player_data;
    };

    self.data = self.copyStatsToRoot(player);

};