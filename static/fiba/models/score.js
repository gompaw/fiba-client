'use strict';

angular.module('fibaclient.models.score', [])
    .factory('Score',
    ['$resource', '$http' , '$interval', '$timeout', 'Alert', function ($resource,$http, $interval, $timeout, Alert) {

        var self = this;

        self.score_data = {};
        self.download_timeout = 1000;
        self.manual = false;
        self.sync = true;

        self.getTeam = function (letter) {
            return self.score_data['team' + letter];
        };

        self.current_time_description = '';

        self.time_descriptions = [
            ' ',
            'koniec 1. kwarty',
            'koniec 1. połowy',
            'po 1. połowie',
            'koniec 3. kwarty',
            'koniec meczu',
            'koniec 4. kwarty',
            'po 4. kwarcie',
            'koniec 1. dogrywki',
            'koniec 2. dogrywki',
            'koniec 3. dogrywki',
            'koniec 4. dogrywki',
            'koniec dogrywki'
        ];

        self.prepareScoreboxControllerData = function(){
            function addTeamQuarters(element, letter){
                for(var i = 0 ; i < self.getTeam(letter).quarters.length ; i++){
                    element['q' + self.getTeam(letter).quarters[i].number + letter.toLowerCase()] = self.getTeam(letter).quarters[i].score;
                }
                element['score' + letter] = self.sumTeamScore(self.getTeam(letter));
            }

            var ret = {
                'teamA.name': self.getTeam('A').full_name,
                'teamB.name': self.getTeam('B').full_name,
                'teamA.shortname': self.getTeam('A').short_name,
                'teamB.shortname': self.getTeam('B').short_name,
                'combo.endstring': self.current_time_description,
                'teamA.score': self.sumTeamScore(self.getTeam('A')).toString(),
                'teamB.score': self.sumTeamScore(self.getTeam('B')).toString(),
                'current.quarter': self.score_data['current_quarter_name']
            };

            ret.current_quarter_name = self.score_data['current_quarter_name'];

            addTeamQuarters(ret, 'A');
            addTeamQuarters(ret, 'B');

            return ret;
        };

        self.downloadScore = function () {

            if(self.manual){
                return;
            }
            if(!self.sync){
                return;
            }
            $resource('/scorebox', {}, {'query': {method: 'GET', isArray: false}}).query(
                {},
                {},
                function(data){
                    self.score_data = data;
                },
                function(data){
                    Alert.addError('Nie udało się pobrać aktualnego wyniku.', self.download_timeout);
                }
            );
        };

        self.sendScoreToServer = function(combo_mode, put){
            if(!self.sync) {
                return;
            }
            try{
                $http({
                    url: combo_mode == undefined?'/scorebox':'/combo',
                    method:  put == undefined?'POST':'PUT',
                    data: self.prepareScoreboxControllerData()
                }).success(function(){
                }).error(function(data, status_code){
                    if(status_code != 404){
                        Alert.addError('Nie udalo sie zaktualizowac comba z wynikami.'+ data.message, 500);
                    }
                });
            }catch(e){
                console.log(e);
            }
        };

        $interval(self.sendScoreToServer, 1000);

        self.refreshScorePeriodically = function () {
            $interval(self.downloadScore, self.download_timeout);
            self.downloadScore();
        };

        self.sumTeamScore = function(team){
            var sum = 0;
            for(var i = 0 ; i < team.quarters.length ; i++){
                sum += parseInt(team.quarters[i].score);
            }
            return sum;
        };

        self.addPointsTo = function(letter, points){
            if(!self.manual){
                return;
            }
            for(var i = 0 ; i <  self.score_data['team' + letter].quarters.length ; i++){
                if(parseInt(self.score_data['team' + letter].quarters[i].number) ==  self.score_data.current_quarter_number){

                    self.score_data['team' + letter].quarters[i].score = parseInt(self.score_data['team' + letter].quarters[i].score) +  parseInt(points);
                }
            }
        };

        self.addPointsToA = function(points){
            return self.addPointsTo('A', points);
        };

        self.addPointsToB = function(points){
            return self.addPointsTo('B', points);
        };

        self.refreshScorePeriodically();

        return {
            getTeamA: function () {
                return self.getTeam('A');
            },
            getTotalScoreForTeamA: function(){
                try{
                    return self.sumTeamScore(self.getTeam('A'));
                }catch(e){
                    return 0;
                }
            },
            getTotalScoreForTeamB: function(){
                try{
                    return self.sumTeamScore(self.getTeam('B'));
                }catch(e){
                    return 0;
                }
            },
            getTeamB: function () {
                return self.getTeam('B');
            },
            getTimeout: function(){
                return self.download_timeout;
            },
            getCurrentQuarter: function(){
                return self.score_data['current_quarter_number'];
            },
            useManualScoring: function(){
                return self.manual;
            },
            setScoringMode: function(mode){
                self.manual = mode;
            },
            setSyncMode: function(mode){
                self.sync = mode;
            },
            useManualSync: function(){
                return self.sync;
            },
            setDescription: function(new_description){
                self.current_time_description = new_description;
            },
            getDescriptionOptions: function(){
                return self.time_descriptions;
            },
            showCombo: function(){
                self.sendScoreToServer(true, true);
            },
            hideCombo: function(){
                $http({
                    url: '/combo',
                    method: 'DELETE',
                    data: {}
                }).success(function(){
                    Alert.addSuccess('Ukryto combo', 500);
                }).error(function(){
                    Alert.addError('Nie udalo sie ukryć comba');
                });
            }, showScorebox: function(){
                self.sendScoreToServer(undefined, true);
            },
            hideScorebox: function(){
                $http({
                    url: '/scorebox',
                    method: 'DELETE',
                    data: {}
                }).success(function(){
                    Alert.addSuccess('Ukryto scorebox', 500);
                }).error(function(){
                    Alert.addError('Nie udalo sie ukryć scoreboxa');
                });
            },
            showFouls: function(){
                if(!self.sync){
                    return;
                }

                function is_numeric(mixed_var) {
                    var whitespace =
                        " \n\r\t\f\x0b\xa0\u2000\u2001\u2002\u2003\u2004\u2005\u2006\u2007\u2008\u2009\u200a\u200b\u2028\u2029\u3000";
                    return (typeof mixed_var === 'number' || (typeof mixed_var === 'string' && whitespace.indexOf(mixed_var.slice(-1)) === -
                            1)) && mixed_var !== '' && !isNaN(mixed_var);
                }

                $resource('/fouls', {}, {'query': {method: 'GET', isArray: false}}).query(
                    {},
                    {},
                    function(data){
                        var max_key = 0;
                        for (var key in data) {
                            if(!is_numeric(key)){
                                continue;
                            }
                            if(key>max_key){
                                max_key = key;
                            }
                        }
                        var elem = data[max_key];
                        elem['fouls_a'] = elem['1'];
                        elem['fouls_b'] = elem['2'];
                        delete elem['1'];
                        delete elem['2'];
                        $http({
                            url: '/fouls_in_quarter',
                            method: 'POST',
                            data: elem
                        }).success(function(){
                        }).error(function(){
                            Alert.addError('Nie udalo sie przeslac info o faulach.');
                        });
                    },
                    function(data){
                        Alert.addError('Nie udało się pobrać fauli');
                    }
                );
            },
            hideFouls: function(){
                $http({
                    url: '/fouls_in_quarter',
                    method: 'DELETE',
                    data: {}
                }).success(function(){
                    Alert.addSuccess('Ukryto faule', 500);
                }).error(function(){
                    Alert.addError('Nie udalo sie ukryć fauli');
                });
            },
            addPointsToA: self.addPointsToA,
            addPointsToB: self.addPointsToB
        }

    }]);