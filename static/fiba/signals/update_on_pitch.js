'use strict';

angular.module('fibaclient.signals.update_on_pitch', [])
    .factory('UpdateOnPitchSignal',
    ['$resource', '$http' , '$interval', '$timeout', 'Alert', 'Match', function ($resource,$http, $interval, $timeout, Alert, Match) {

        var self = {};

        self.teamA = {};
        self.teamB = {};

        self.teamA = Match.getTeamA();
        self.teamB = Match.getTeamB();

        self.generateStats = function(team_letter){
            function attachPlayers(element, team){
                var players_sorted = self['team'+team].players.sort(function(a, b) {
                if(a.in_field === b.in_field)
                    return a.number-b.number;
                else if(a.in_field)
                    return -1;
                else return 1;
                });
                element['playerB.surname'] =  players_sorted[1].number + '. ' +  players_sorted[1].last_name;
                element['playerC.surname'] =  players_sorted[2].number + '. ' +  players_sorted[2].last_name;
                element['playerD.surname'] =  players_sorted[3].number + '. ' +  players_sorted[3].last_name;
                element['playerE.surname'] =  players_sorted[4].number + '. ' +  players_sorted[4].last_name;
                element['playerA.surname'] =  players_sorted[0].number + '. ' +  players_sorted[0].last_name;

/*                for(var i = 0 ; i < 14 ; i ++){
                    var player = self['team'+team].players[i];

                    element['player'+(i+1)+'.name'] = ' ';
                    element['player'+(i+1)+'.firstletter'] = ' ';
                    element['player'+(i+1)+'.surname'] = ' ';
                    element['player'+(i+1)+'.height'] = ' ';
                    element['player'+(i+1)+'.position'] = ' ';
                    element['player'+(i+1)+'.age'] = ' ';
                    element['player'+(i+1)+'.number'] = ' ';
                    element['player'+(i+1)+'.fullname'] = ' ';

                    if(self['team'+team].players[i] != undefined){
                        element['player'+(i+1)+'.name'] = player.first_name;
                        element['player'+(i+1)+'.firstletter'] = player.first_name[0];
                        element['player'+(i+1)+'.surname'] = player.last_name;
                        element['player'+(i+1)+'.fullname'] = player.first_name + ' ' + player.last_name;
                        element['player'+(i+1)+'.height'] = player.height?player.height.toFixed(2):" ";
                        element['player'+(i+1)+'.position'] = player.position;
                        element['player'+(i+1)+'.age'] = player.age?player.age:" ";
                        element['player'+(i+1)+'.number'] = player.number;
                    }

                } */
            }
            var ret = {
            };
            
            if(team_letter == 'a'){
                ret = {
                    'team.name': self.teamA.name,
                    'team.shortname': self.teamA.short_name,
                    'team.coach': self.teamA.coach,
                    'team.assistantcoaches': self.teamA.assistant_coaches,
                    "logo": team_letter.toUpperCase(),
                };
                attachPlayers(ret, 'A');
            }else{
                ret = {
                    'team.name': self.teamB.name,
                    'team.shortname': self.teamB.short_name,
                    'team.coach': self.teamB.coach,
                    'team.assistantcoaches': self.teamB.assistant_coaches,
                    "logo": team_letter.toUpperCase(),
                };
                attachPlayers(ret, 'B');
            }
            return ret;
        };

        return {
            setTeamA: function(data){
                self.teamA = data;
            },
            setTeamB: function(data){
                self.teamB = data;
            },

            show: function(team_letter){
                var ret = self.generateStats(team_letter);
                $http({
                    url: '/on_pitch',
                    method: 'POST',
                    data: ret
                }).success(function(){
                    Alert.addSuccess('Skład został pomyślnie wygenerowany.', 2000)
                }).error(function(data){
                    Alert.addError('Nie udało się wygenerować składów:  ' + data.message)
                });
            },
            update: function(team_letter){
                var ret = self.generateStats(team_letter);
                $http({
                    url: '/on_pitch',
                    method: 'PATCH',
                    data: ret
                }).success(function(){
                    Alert.addSuccess('Skład został pomyślnie wygenerowany.', 2000)
                }).error(function(data){
                    Alert.addError('Nie udało się wygenerować składów:  ' + data.message)
                });
            }
            , hide: function(){

                $http({
                    url: '/on_pitch',
                    method: 'DELETE'
                }).success(function(){
                    Alert.addSuccess('Skład został pomyślnie wyczyszczony.', 2000)
                }).error(function(data){
                    Alert.addError('Nie udało się wyczyscic składów:  ' + data.message)
                });
            }
        }

    }]);