'use strict';

angular.module('fibaclient.signals.update_squad', [])
    .factory('UpdateSquadSignal',
    ['$resource', '$http' , '$interval', '$timeout', 'Alert', 'Match', function ($resource,$http, $interval, $timeout, Alert, Match) {

        var self = {};

        self.teamA = {};
        self.teamB = {};

        self.teamA = Match.getTeamA();
        self.teamB = Match.getTeamB();

        self.generateStats = function(team_letter){
            function attachPlayers(element, team){
                for(var i = 0 ; i < 14 ; i ++){
                    var player = self['team'+team].players[i];

                    element['player'+(i+1)+'.name'] = ' ';
                    element['player'+(i+1)+'.firstletter'] = ' ';
                    element['player'+(i+1)+'.surname'] = ' ';
                    element['player'+(i+1)+'.height'] = ' ';
                    element['player'+(i+1)+'.position'] = ' ';
                    element['player'+(i+1)+'.age'] = ' ';
                    element['player'+(i+1)+'.number'] = ' ';
                    element['player'+(i+1)+'.fullname'] = ' ';

                    if(self['team'+team].players[i] != undefined){
                        element['player'+(i+1)+'.name'] = player.first_name;
                        element['player'+(i+1)+'.firstletter'] = player.first_name[0];
                        element['player'+(i+1)+'.surname'] = player.last_name;
                        element['player'+(i+1)+'.fullname'] = player.first_name + ' ' + player.last_name;
                        element['player'+(i+1)+'.height'] = player.height?player.height.toFixed(2):" ";
                        element['player'+(i+1)+'.position'] = player.position;
                        element['player'+(i+1)+'.age'] = player.age?player.age:" ";
                        element['player'+(i+1)+'.number'] = player.number;
                    }

                }
            }
            var ret = {
            };
            
            if(team_letter == 'a'){
                ret = {
                    'team.name': self.teamA.name,
                    'team.shortname': self.teamA.short_name,
                    'team.coach': self.teamA.coach,
                    'team.assistantcoaches': self.teamA.assistant_coaches,
                    "logo": team_letter.toUpperCase(),
                };
                attachPlayers(ret, 'A');
            }else{
                ret = {
                    'team.name': self.teamB.name,
                    'team.shortname': self.teamB.short_name,
                    'team.coach': self.teamB.coach,
                    'team.assistantcoaches': self.teamB.assistant_coaches,
                    "logo": team_letter.toUpperCase(),
                };
                attachPlayers(ret, 'B');
            }
            return ret;
        };

        return {
            setTeamA: function(data){
                self.teamA = data;
            },
            setTeamB: function(data){
                self.teamB = data;
            },

            show: function(team_letter){
                var ret = self.generateStats(team_letter);
                $http({
                    url: '/squads',
                    method: 'POST',
                    data: ret
                }).success(function(){
                    Alert.addSuccess('Skład został pomyślnie wygenerowany.', 2000)
                }).error(function(data){
                    Alert.addError('Nie udało się wygenerować składów:  ' + data.message)
                });
            },
            update: function(team_letter){
                var ret = self.generateStats(team_letter);
                $http({
                    url: '/squads',
                    method: 'PATCH',
                    data: ret
                }).success(function(){
                    Alert.addSuccess('Skład został pomyślnie wygenerowany.', 2000)
                }).error(function(data){
                    Alert.addError('Nie udało się wygenerować składów:  ' + data.message)
                });
            }
            , hide: function(){

                $http({
                    url: '/squads',
                    method: 'DELETE'
                }).success(function(){
                    Alert.addSuccess('Skład został pomyślnie wyczyszczony.', 2000)
                }).error(function(data){
                    Alert.addError('Nie udało się wyczyscic składów:  ' + data.message)
                });
            }
        }

    }]);