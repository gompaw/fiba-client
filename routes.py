from resources.players import PlayerResource, PlayersResource
from resources.teams import TeamsResource
from resources.match import MatchResource
from resources.scorebox import ScoreBoxResource
from resources.player_signature_bar import PlayersSignatureBarResource
from resources.referee_signature_bar import RefereeSignatureBarResource
from resources.on_pitch import OnPitchResource
from resources.top5 import Top5Resource
from resources.squads import SquadsResource
from resources.first5 import First5Resource
from resources.combo import ComboResource
from resources.small_statistics import SmallStatisticsResource
from resources.small_statistics_hist import SmallStatisticsHistResource
from resources.coach_signature_bar import CoachSignatureBarResource
from resources.huge_statistics import HugeStatisticsResource
from resources.player_statistics_bar import PlayersStatisticsBarResource
from resources.player_comp import PlayerCompResource
from resources.medium_statistics_1 import MediumStatisticsResource1
from resources.medium_statistics_2 import MediumStatisticsResource2
from resources.medium_statistics_3 import MediumStatisticsResource3
from resources.medium_statistics_1_hist import MediumStatisticsHistResource1
from resources.medium_statistics_2_hist import MediumStatisticsHistResource2
from resources.medium_statistics_3_hist import MediumStatisticsHistResource3
from resources.fouls_in_quarter import ComboFoulsResource
from resources.fouls import FoulsResource
from resources.history_storage import HistoryStorageResource

routes = {
    PlayersResource: '/players',
    PlayerResource: '/player/<int:player_id>',
    TeamsResource: '/teams',
    MatchResource: '/match',
    PlayersSignatureBarResource: '/player_signature_bar',
    RefereeSignatureBarResource: '/referee_signature_bar',
    Top5Resource:'/top5',
    SquadsResource: '/squads',
    First5Resource: '/first5',
    ComboResource: '/combo',
    SmallStatisticsResource: '/small_statistics',
    SmallStatisticsHistResource: '/small_statistics_hist',
    CoachSignatureBarResource: '/coach_signature_bar',
    HugeStatisticsResource: '/huge_statistics',
    ScoreBoxResource: '/scorebox',
    PlayerCompResource: '/player_comp',
    PlayersStatisticsBarResource: '/player_statistics_bar',
    OnPitchResource: '/on_pitch',
    MediumStatisticsResource1: '/medium_stat_1',
    MediumStatisticsResource2: '/medium_stat_2',
    MediumStatisticsResource3: '/medium_stat_3',
    MediumStatisticsHistResource1: '/medium_stat_1_hist',
    MediumStatisticsHistResource2: '/medium_stat_2_hist',
    MediumStatisticsHistResource3: '/medium_stat_3_hist',
    ComboFoulsResource: '/fouls_in_quarter',
    FoulsResource: '/fouls',
    HistoryStorageResource: '/history',
}
