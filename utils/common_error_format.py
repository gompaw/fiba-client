from libs.Config import Config
import traceback


def common_error(message):
    if Config.get("debug_mode"):
        print(traceback.format_exc())
    return {"message": message}