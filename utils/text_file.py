from utils.http_status_codes import STATUS_BAD_REQUEST_400, STATUS_OK_200
from utils import common_error


class TextFile:

    disallowed_values = [None, '']
    ignored_rows = []

    accumulated_data = {}

    @staticmethod
    def create_row(key, value):
        if key in TextFile.ignored_rows:
            raise TextFile.IncorrectInputData("Klucz " + (key) + " jest na liście ignorowanych")
        if value in TextFile.disallowed_values:
            raise TextFile.IncorrectInputData("Wartość " + str(value) + " jest na liście ignorowanych")
        TextFile.accumulated_data[key] = value
        return str(key) + ' = ' + str(value) + '\n'

    class IncorrectInputData(Exception):
        pass

    class FileNotFound(Exception):
        pass

    @staticmethod
    def save_data(data, prefix, filename):
        try:
            f = open(filename, 'w+')
            for key in data.keys():
                try:
                    f.write(TextFile.create_row(prefix+'.'+key, data[key]))
                except TextFile.IncorrectInputData:
                    continue
            f.close()
            return TextFile.accumulated_data, STATUS_OK_200
        except Exception as e:
            return common_error(str(e)), STATUS_BAD_REQUEST_400
