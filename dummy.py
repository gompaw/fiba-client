

from random import randint


def get_dummy_team_a():
    from models import Team
    team = Team()
    team.coach = "Marcin Kobylański"
    team.score_q4 = 78
    team.score_q3 = 32
    team.score_q2 = 15
    team.score_q1 = 5
    team.short_name = "TZG"
    team.name = "Turów Zgorzelec"
    return team


def get_dummy_team_b():
    from models import Team
    team = Team()
    team.coach = "Monika Łacutkiewicz"
    team.score_q4 = 73
    team.score_q3 = 31
    team.score_q2 = 13
    team.score_q1 = 6
    team.short_name = "PTS"
    team.name = "Prokom Trefl Sopot"
    return team

names = ["Marcin", "Paweł", "Oskar", "Janusz", "Piotr", "Kostek", "Jędrzej", "Krzysztof", "Bartosz", "Ziemowit",
         "Aureliusz"]

surnames = ["Grzegorzewski","Bartczak","Ziomowski","Krzynówek","Opania","Sobczyk","Sobkowiak","Grzymanowski",
            "Ziomeczkowski"]


def get_dummy_player():
    from models import Player
    player = Player()
    player.first_name = names[randint(0, len(names) - 1)]
    player.last_name = surnames[randint(0, len(surnames) - 1)]
    player.number = randint(0, 100)
    return player