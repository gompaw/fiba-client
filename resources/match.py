from flask_restful import Resource
from models import Match
from utils import common_error
from utils.http_status_codes import STATUS_BAD_REQUEST_400
from os import makedirs, path, unlink
from libs.Config import Config
import json


class MatchResource(Resource):

    def __init__(self):
        self.target_path = Config.get('appdir')

    def get(self):
        try:
            match = Match()
        except Exception as e:
            try:
                return self._load_history_or_throw_error()
            except Exception as f:
                return common_error(str(f)), STATUS_BAD_REQUEST_400
        self._save(json.dumps(match.to_dict()))
        return match.to_dict()

    def _create_missing_dirs(self):
        try:
            makedirs(self.target_path)
        except FileExistsError:
            pass

    def _save(self, json_payload):
        with open(self._get_target_file_name(), "w") as fp:
            fp.write(json_payload)

    def _get_target_file_name(self):
        return path.join(
            self.target_path,
            "stats.json"
        )

    def _load_history_or_throw_error(self):
        try:
            with open(self._get_target_file_name(), "r") as fp:
                return json.load(fp)
        except FileNotFoundError:
            raise MatchResource.DoesNotExist("Brak kopi zapasowej danych statystycznych.")

    class DoesNotExist(Exception):
        pass
