from flask_restful import Resource
from models import Team


class TeamsResource(Resource):

    def get(self):
        team = Team()
        return team.to_dict()
