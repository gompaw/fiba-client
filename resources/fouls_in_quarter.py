from flask_restful import Resource, request
from libs.Config import Config
from libs.xmlgenerator import XmlGenerator
from libs.casparcgcomm import CasparCGConnection

class ComboFoulsResource(Resource):

    def post(self):
        CasparCGConnection.updateCG(1,8,XmlGenerator.get_xml(request.get_json()))

    def put(self):
        CasparCGConnection.addCG(1,8,Config.get("combo-fouls_template"),XmlGenerator.get_xml(request.get_json()))

    def delete(self):
         CasparCGConnection.stopCG(1,8)