from flask_restful import Resource, request
from libs.Config import Config
from libs.xmlgenerator import XmlGenerator
from libs.casparcgcomm import CasparCGConnection


class SmallStatisticsResource(Resource):

    def post(self):
        CasparCGConnection.addCG(1,9,Config.get("combo-stat_template"),XmlGenerator.get_xml(request.get_json()))

    def delete(self):
         CasparCGConnection.stopCG(1,9)