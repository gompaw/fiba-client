from flask_restful import Resource, request
from libs.Config import Config
from libs.xmlgenerator import XmlGenerator
from libs.casparcgcomm import CasparCGConnection


class MediumStatisticsHistResource3(Resource):
    def post(self):
        CasparCGConnection.addCG(1,20,Config.get("medium_stat_3_hist_template"),XmlGenerator.get_xml(request.get_json()))
        CasparCGConnection.playVideo(1,10,Config.get("medium_stat_bkg"))

    def patch(self):
        CasparCGConnection.updateCG(1,20,XmlGenerator.get_xml(request.get_json()))

    def delete(self):
        CasparCGConnection.stopCG(1,20)
        CasparCGConnection.stopVideo(1,10)