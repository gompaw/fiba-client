from flask_restful import Resource, request
from models import ScoreBox
from utils import common_error
from utils.http_status_codes import STATUS_BAD_REQUEST_400
from libs.xmlgenerator import XmlGenerator
from libs.casparcgcomm import CasparCGConnection
from libs.Config import Config


class ScoreBoxResource(Resource):

    def get(self):
        try:
            score = ScoreBox()
        except Exception as e:
            return common_error(str(e)), STATUS_BAD_REQUEST_400
        return score.to_dict()

    def post(self):
        CasparCGConnection.addCG(1,20,Config.get("scorebox_template"),XmlGenerator.get_xml(request.get_json()))

    def put(self):
        CasparCGConnection.addCG(1,20,Config.get("scorebox_template"),XmlGenerator.get_xml(request.get_json()))
        CasparCGConnection.playVideo(1,10,Config.get("scorebox_bkg"))


    def delete(self):
        CasparCGConnection.stopCG(1,20)
        CasparCGConnection.stopVideo(1,10)