from flask_restful import Resource, request
from libs.Config import Config
from libs.xmlgenerator import XmlGenerator
from libs.casparcgcomm import CasparCGConnection


class RefereeSignatureBarResource(Resource):

    def post(self):
        CasparCGConnection.addCG(1,20,Config.get("referee_template"),XmlGenerator.get_xml(request.get_json()))
        CasparCGConnection.playVideo(1,10,Config.get("referee_bkg"))


    def delete(self):
        CasparCGConnection.stopCG(1,20)
        CasparCGConnection.stopVideo(1,10)

    def patch(self):
        CasparCGConnection.updateCG(1,20,XmlGenerator.get_xml(request.get_json()))