from flask_restful import Resource, request
from utils import TextFile
from libs.Config import Config


class PlayersStatisticsBarResource(Resource):

    def post(self):
        TextFile.save_data(data=request.get_json(), prefix="playerstat", filename=Config.get("player_statistics_bar_file_location"))
