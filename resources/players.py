from flask_restful import Resource
from models import Player


class PlayersResource(Resource):

    def get(self):
        player = Player()
        return player.to_dict()


class PlayerResource(Resource):

    def get(self, player_id):
        return Player.get(Player.id == player_id).to_dict()
