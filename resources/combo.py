from flask_restful import Resource, request
from libs.Config import Config
from libs.xmlgenerator import XmlGenerator
from libs.casparcgcomm import CasparCGConnection
import time


class ComboResource(Resource):

    def post(self):
        CasparCGConnection.updateCG(1,30,XmlGenerator.get_xml(request.get_json()))

    def put(self):
        CasparCGConnection.addCG(1,30,Config.get("combo_template"),XmlGenerator.get_xml(request.get_json()))
        CasparCGConnection.playVideo(1,11,Config.get("combo_bkg"))
        time.sleep(1)
        CasparCGConnection.layerOpacity(1,61, 1, 30)
        CasparCGConnection.layerOpacity(1,62, 1, 30)
        CasparCGConnection.addCG(1,8,Config.get("combo_fouls_template"),XmlGenerator.get_xml(request.get_json()))

    def delete(self):
        CasparCGConnection.stopCG(1,30)
        CasparCGConnection.stopCG(1,9)
        CasparCGConnection.stopVideo(1,11)
        CasparCGConnection.stopCG(1,8)
        CasparCGConnection.layerOpacity(1,61, 0, 30)
        CasparCGConnection.layerOpacity(1,62, 0, 30)