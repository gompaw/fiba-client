from flask_restful import Resource, request
from models.foul import Foul


class FoulsResource(Resource):

    def get(self):
        f = Foul()
        return f.data
