from flask_restful import Resource, request
from utils import common_error
from utils.http_status_codes import STATUS_BAD_REQUEST_400
from models.historical_data import HistoricalData
import json


class HistoryStorageResource(Resource):
    """
    Stores historical data from previous matches on the disk
    """
    def get(self):
        try:
            return json.loads(HistoricalData.load())
        except Exception as e:
            return common_error(str(e)), STATUS_BAD_REQUEST_400

    def post(self):
        try:
            return HistoricalData.save(request.get_data(as_text=True))
        except Exception as e:
            return common_error(str(e)), STATUS_BAD_REQUEST_400

    def delete(self):
        try:
            return HistoricalData.delete()
        except Exception as e:
            return common_error(str(e)), STATUS_BAD_REQUEST_400

    def patch(self):
        try:
            return HistoricalData.save(request.get_data(as_text=True))
        except Exception as e:
            return common_error(str(e)), STATUS_BAD_REQUEST_400
